create table public.users (
	login varchar not null,
	password varchar default null,
	constraint users_login_pk primary key (username)
);

insert into users (login, password)
  values ('admin', 'password');
insert into users (login, password)
  values ('user', '123456');