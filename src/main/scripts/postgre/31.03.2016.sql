ALTER TABLE attributes
   ALTER COLUMN atr_type TYPE bigint;
ALTER TABLE attr_types
	DROP CONSTRAINT attr_types_attribute_id_pk;
ALTER TABLE attr_types
  ADD CONSTRAINT attr_types_attribute_id_type_id_pk PRIMARY KEY (attribute_id, type_id);
