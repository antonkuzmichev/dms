insert into types (type_id, name, description)
  values (1, 'Phones', 'Telecommunications device that permits two or more users to conduct a conversation when they are too far apart to be heard directly.');
insert into types (type_id, name, description)
  values (2, 'Laptops, tablets and PCs', 'General purpose device that can be programmed to carry out a set of arithmetic or logical operations automatically.');
insert into types (type_id, name, description)
  values (3, 'TV and Video', 'elevision is a telecommunication medium used for transmitting sound with moving images in monochrome (black-and-white), or in color, and in two or three dimensions.');
insert into types (type_id, name, description)
  values (4, 'Home Appliances', 'Home appliances are electrical/mechanical machines which accomplish some household functions, such as cooking or cleaning. ');


insert into list_value_type (list_value_type_id, name)
  values (1, 'Scope');
insert into list_value_type (list_value_type_id, name)
  values (2, 'Age category');
insert into list_value_type (list_value_type_id, name)
  values (3, 'Warranty');


insert into list_value (list_value_id, list_value_type_id, value)
  values (1, 1, 'House');
insert into list_value (list_value_id, list_value_type_id, value)
  values (2, 1, 'Company');
insert into list_value (list_value_id, list_value_type_id, value)
  values (3, 2, 'Children');
insert into list_value (list_value_id, list_value_type_id, value)
  values (4, 2, 'Adult');
insert into list_value (list_value_id, list_value_type_id, value)
  values (5, 3, 'Year');
insert into list_value (list_value_id, list_value_type_id, value)
  values (6, 3, 'Month');


insert into attributes (attribute_id, name, atr_type, description)
  values (1, 'Color', 1,'The visual perceptual property corresponding in humans to the categories called red, yellow, white, etc.');
insert into attributes (attribute_id, name, atr_type, description)
  values (2, 'Model', 2,'A product is anything that can be offered to a market that might satisfy a want or need.');
insert into attributes (attribute_id, name, atr_type, description)
  values (3, 'Type of sensor', 3,'Censor gives a measure of control over the cursor.');
insert into attributes (attribute_id, name, atr_type, description)
  values (4, 'Brand name', 4,'Name, term, design, symbol or other feature that distinguishes one seller`s product from those of others.');


insert into attr_types (attribute_id, type_id)
  values (1, 1);
insert into attr_types (attribute_id, type_id)
  values (4, 3);
insert into attr_types (attribute_id, type_id)
  values (2, 4);
insert into attr_types (attribute_id, type_id)
  values (3, 2);


insert into attr_list_value (attribute_id, list_value_type_id)
  values (1, 1);
insert into attr_list_value (attribute_id, list_value_type_id)
  values (3, 2);
insert into attr_list_value (attribute_id, list_value_type_id)
  values (2, 2);
insert into attr_list_value (attribute_id, list_value_type_id)
  values (4, 3);


insert into objects (object_id, type_id, name, parent_id, description, project_id, order_id)
  values (1, 2, 'Monitor', 1, 'Structurally complete device for displaying visual information.', 1, 1);
insert into objects (object_id, type_id, name, parent_id, description, project_id, order_id)
  values (2, 4, 'Food processor', 2, 'A kitchen appliance used to facilitate repetitive tasks in the preparation of food.', 2, 2);
insert into objects (object_id, type_id, name, parent_id, description, project_id, order_id)
  values (3, 1, 'Smartphone', 3, 'A mobile phone with an advanced mobile operating system which combines features of a personal computer operating system with other features useful for mobile or handheld use.', 3, 3);
insert into objects (object_id, type_id, name, parent_id, description, project_id, order_id)
  values (4, 2, 'Keyboard', 4, 'A typewriter-style device which uses an arrangement of buttons or keys to act as a mechanical lever or electronic switch.', 4, 1);
insert into objects (object_id, type_id, name, parent_id, description, project_id, order_id)
  values (5, 4, 'Juicer', 5, ' A tool for separating juice from fruits, herbs, leafy greens and other types of vegetables from its pulp in a process called juicing. ', 5, 4);
insert into objects (object_id, type_id, name, parent_id, description, project_id, order_id)
  values (6, 3, 'Xbox', 6, 'A video gaming brand created and owned by Microsoft.', 6, 5);


insert into values (object_id, list_value_id, attribute_id, value, date)
  values (1, 2, 2, 'S24D300H', '2013-05-18');
insert into values (object_id, list_value_id, attribute_id, value, date)
  values (2, 1, 4, 'Philips Daily Collection', '2014-10-21');
insert into values (object_id, list_value_id, attribute_id, value, date)
  values (5, 5, 1, 'Grey', '2014-09-03');
insert into values (object_id, list_value_id, attribute_id, value, date)
  values (6, 3, 2, '500Gb (5C5-00015)', '2015-01-07');

insert into public.references (object_id, reference, attribute_id, order_1)
  values (2, 1, 4, 2);
insert into public.references (object_id, reference, attribute_id, order_1)
  values (4, 2, 3, 1);
insert into public.references (object_id, reference, attribute_id, order_1)
  values (5, 3, 1, 4);
insert into public.references (object_id, reference, attribute_id, order_1)
  values (6, 4, 2, 5);
