CREATE TABLE documents_def
(
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR,
    hash VARCHAR
);
CREATE UNIQUE INDEX documents_def_id_uindex ON documents_def (id);


CREATE TABLE docs_changes
(
    doc_id BIGINT,
    attribute_id BIGINT,
    type_id BIGINT,
    list_value_type_id BIGINT,
    list_value_id BIGINT,
    CONSTRAINT docs_changes_docs_fk FOREIGN KEY (doc_id) REFERENCES documents_def (id),
    CONSTRAINT docs_changes_attr__fk FOREIGN KEY (attribute_id) REFERENCES attributes (attribute_id),
    CONSTRAINT docs_changes__type_fk FOREIGN KEY (type_id) REFERENCES types (type_id),
    CONSTRAINT docs_changes_listvaluetype__fk FOREIGN KEY (list_value_type_id) REFERENCES list_value_type (list_value_type_id),
    CONSTRAINT docs_changes_listvalues__fk FOREIGN KEY (list_value_id) REFERENCES list_values (list_value_id)
);