--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5rc1
-- Dumped by pg_dump version 9.5rc1

-- Started on 2016-04-21 23:03:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 193 (class 1259 OID 41973)
-- Name: object_references; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE object_references (
    ref_id bigint NOT NULL,
    object_id bigint NOT NULL,
    object_ref_id bigint NOT NULL,
    ref_order bigint NOT NULL,
    ref_attribute bigint
);


--
-- TOC entry 2022 (class 2606 OID 41977)
-- Name: object_references_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT object_references_pkey PRIMARY KEY (ref_id);


--
-- TOC entry 2023 (class 1259 OID 41993)
-- Name: object_references_ref_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX object_references_ref_id_uindex ON object_references USING btree (ref_id);


--
-- TOC entry 2026 (class 2606 OID 41988)
-- Name: attribute_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT attribute_fk FOREIGN KEY (ref_attribute) REFERENCES attributes(attribute_id);


--
-- TOC entry 2027 (class 2606 OID 41994)
-- Name: fka3meux9gl6luwmq9jjx4tne8r; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT fka3meux9gl6luwmq9jjx4tne8r FOREIGN KEY (ref_attribute) REFERENCES attributes(attribute_id);


--
-- TOC entry 2028 (class 2606 OID 41999)
-- Name: fkfcbpnsc5s2hujg0vv0h58u0id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT fkfcbpnsc5s2hujg0vv0h58u0id FOREIGN KEY (object_id) REFERENCES objects(object_id);


--
-- TOC entry 2030 (class 2606 OID 42014)
-- Name: fkfg7q503m58cflaacw90r5cvbp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT fkfg7q503m58cflaacw90r5cvbp FOREIGN KEY (ref_attribute) REFERENCES attributes(attribute_id);


--
-- TOC entry 2029 (class 2606 OID 42004)
-- Name: fkfiw4ovhtmgeo1namdxrp412sr; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT fkfiw4ovhtmgeo1namdxrp412sr FOREIGN KEY (object_ref_id) REFERENCES objects(object_id);


--
-- TOC entry 2024 (class 2606 OID 41978)
-- Name: object_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT object_fk FOREIGN KEY (object_id) REFERENCES objects(object_id);


--
-- TOC entry 2025 (class 2606 OID 41983)
-- Name: object_ref_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT object_ref_fk FOREIGN KEY (object_ref_id) REFERENCES objects(object_id);


-- Completed on 2016-04-21 23:03:13

--
-- PostgreSQL database dump complete
--

