--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5rc1
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-03-22 17:18:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 191 (class 1259 OID 17162)
-- Name: data_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE data_types (
    id bigint NOT NULL,
    name text
);


--
-- TOC entry 2132 (class 0 OID 17162)
-- Dependencies: 191
-- Data for Name: data_types; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO data_types (id, name) VALUES (1, 'Integer');
INSERT INTO data_types (id, name) VALUES (2, 'String');
INSERT INTO data_types (id, name) VALUES (3, 'List');
INSERT INTO data_types (id, name) VALUES (4, 'Reserved');


--
-- TOC entry 2017 (class 2606 OID 17171)
-- Name: data_type_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY data_types
    ADD CONSTRAINT data_type_id PRIMARY KEY (id);


-- Completed on 2016-03-22 17:18:19

--
-- PostgreSQL database dump complete
--

