delete from values;
drop TABLE values;
CREATE TABLE public.values
(
  value_id BIGINT PRIMARY KEY NOT NULL,
  object_id BIGINT NOT NULL,
  attribute_id BIGINT NOT NULL,
  value_int BIGINT,
  value_string VARCHAR,
  value_date DATE,
  value_list_type BIGINT,
  CONSTRAINT values_objects_object_id_fk FOREIGN KEY (object_id) REFERENCES objects (object_id),
  CONSTRAINT values_attributes_attribute_id_fk FOREIGN KEY (attribute_id) REFERENCES attributes (attribute_id),
  CONSTRAINT values_list_value_type_list_value_type_id_fk FOREIGN KEY (value_list_type) REFERENCES list_value_type (list_value_type_id)
);
CREATE UNIQUE INDEX values_value_id_uindex ON public.values (value_id);