--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.0

-- Started on 2016-05-20 17:00:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 194 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2202 (class 0 OID 0)
-- Dependencies: 194
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 184 (class 1259 OID 33602)
-- Name: attr_list_value; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE attr_list_value (
    attribute_id bigint NOT NULL,
    list_value_type_id bigint NOT NULL
);


--
-- TOC entry 185 (class 1259 OID 33617)
-- Name: attr_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE attr_types (
    attribute_id bigint NOT NULL,
    type_id bigint NOT NULL
);


--
-- TOC entry 188 (class 1259 OID 33765)
-- Name: attribute_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE attribute_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- TOC entry 183 (class 1259 OID 33594)
-- Name: attributes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE attributes (
    attribute_id bigint NOT NULL,
    name character varying NOT NULL,
    atr_type bigint NOT NULL,
    description character varying NOT NULL,
    properties character varying
);


--
-- TOC entry 187 (class 1259 OID 33698)
-- Name: data_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE data_types (
    id bigint NOT NULL,
    name text
);


--
-- TOC entry 193 (class 1259 OID 42048)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hibernate_sequence
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- TOC entry 180 (class 1259 OID 33565)
-- Name: list_value_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE list_value_type (
    list_value_type_id bigint NOT NULL,
    name character varying NOT NULL
);


--
-- TOC entry 181 (class 1259 OID 33573)
-- Name: list_values; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE list_values (
    list_value_id bigint NOT NULL,
    list_value_type_id bigint NOT NULL,
    value character varying NOT NULL
);


--
-- TOC entry 191 (class 1259 OID 41973)
-- Name: object_references; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE object_references (
    ref_id bigint NOT NULL,
    object_id bigint NOT NULL,
    object_ref_id bigint NOT NULL,
    ref_order bigint NOT NULL,
    ref_attribute bigint
);


--
-- TOC entry 186 (class 1259 OID 33632)
-- Name: objects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE objects (
    object_id bigint NOT NULL,
    type_id bigint NOT NULL,
    name character varying NOT NULL,
    parent_id bigint NOT NULL,
    description character varying NOT NULL,
    project_id bigint NOT NULL,
    order_id bigint NOT NULL
);


--
-- TOC entry 190 (class 1259 OID 41949)
-- Name: reference_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE reference_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- TOC entry 189 (class 1259 OID 33767)
-- Name: type_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE type_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


--
-- TOC entry 182 (class 1259 OID 33586)
-- Name: types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE types (
    type_id bigint NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    properties character varying
);


--
-- TOC entry 192 (class 1259 OID 42024)
-- Name: values; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "values" (
    value_id bigint NOT NULL,
    object_id bigint NOT NULL,
    attribute_id bigint NOT NULL,
    value_int bigint,
    value_string character varying,
    value_date timestamp without time zone,
    value_listvalue_id bigint
);


--
-- TOC entry 2185 (class 0 OID 33602)
-- Dependencies: 184
-- Data for Name: attr_list_value; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO attr_list_value (attribute_id, list_value_type_id) VALUES (4, 4);


--
-- TOC entry 2186 (class 0 OID 33617)
-- Dependencies: 185
-- Data for Name: attr_types; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO attr_types (attribute_id, type_id) VALUES (4, 3);
INSERT INTO attr_types (attribute_id, type_id) VALUES (2, 4);
INSERT INTO attr_types (attribute_id, type_id) VALUES (3, 2);
INSERT INTO attr_types (attribute_id, type_id) VALUES (4, 1);
INSERT INTO attr_types (attribute_id, type_id) VALUES (1, 1);
INSERT INTO attr_types (attribute_id, type_id) VALUES (2, 1);
INSERT INTO attr_types (attribute_id, type_id) VALUES (3, 1);
INSERT INTO attr_types (attribute_id, type_id) VALUES (6, 8);
INSERT INTO attr_types (attribute_id, type_id) VALUES (7, 8);
INSERT INTO attr_types (attribute_id, type_id) VALUES (1, 28);
INSERT INTO attr_types (attribute_id, type_id) VALUES (4, 28);
INSERT INTO attr_types (attribute_id, type_id) VALUES (3, 28);


--
-- TOC entry 2203 (class 0 OID 0)
-- Dependencies: 188
-- Name: attribute_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('attribute_seq', 14, true);


--
-- TOC entry 2184 (class 0 OID 33594)
-- Dependencies: 183
-- Data for Name: attributes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO attributes (attribute_id, name, atr_type, description, properties) VALUES (1, 'Color', 2, 'The visual perceptual property corresponding in humans to the categories called red, yellow, white, etc.', NULL);
INSERT INTO attributes (attribute_id, name, atr_type, description, properties) VALUES (4, 'Brand name', 4, 'Name, term, design, symbol or other feature that distinguishes one seller`s product from those of others.', '');
INSERT INTO attributes (attribute_id, name, atr_type, description, properties) VALUES (2, 'Model', 1, 'A product is anything that can be offered to a market that might satisfy a want or need.', '');
INSERT INTO attributes (attribute_id, name, atr_type, description, properties) VALUES (3, 'Type of sensor', 2, 'Censor gives a measure of control over the cursor.', '');
INSERT INTO attributes (attribute_id, name, atr_type, description, properties) VALUES (6, 'Login', 2, 'user login for project security', '');
INSERT INTO attributes (attribute_id, name, atr_type, description, properties) VALUES (7, 'Password', 2, 'user password for project security', '');


--
-- TOC entry 2188 (class 0 OID 33698)
-- Dependencies: 187
-- Data for Name: data_types; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO data_types (id, name) VALUES (1, 'Integer');
INSERT INTO data_types (id, name) VALUES (2, 'String');
INSERT INTO data_types (id, name) VALUES (3, 'Date');
INSERT INTO data_types (id, name) VALUES (4, 'List');


--
-- TOC entry 2204 (class 0 OID 0)
-- Dependencies: 193
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('hibernate_sequence', 73, true);


--
-- TOC entry 2181 (class 0 OID 33565)
-- Dependencies: 180
-- Data for Name: list_value_type; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO list_value_type (list_value_type_id, name) VALUES (1, 'Scope');
INSERT INTO list_value_type (list_value_type_id, name) VALUES (2, 'Age category');
INSERT INTO list_value_type (list_value_type_id, name) VALUES (3, 'Warranty');
INSERT INTO list_value_type (list_value_type_id, name) VALUES (4, 'Brands');


--
-- TOC entry 2182 (class 0 OID 33573)
-- Dependencies: 181
-- Data for Name: list_values; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO list_values (list_value_id, list_value_type_id, value) VALUES (1, 1, 'House');
INSERT INTO list_values (list_value_id, list_value_type_id, value) VALUES (2, 1, 'Company');
INSERT INTO list_values (list_value_id, list_value_type_id, value) VALUES (3, 2, 'Children');
INSERT INTO list_values (list_value_id, list_value_type_id, value) VALUES (4, 2, 'Adult');
INSERT INTO list_values (list_value_id, list_value_type_id, value) VALUES (5, 3, 'Year');
INSERT INTO list_values (list_value_id, list_value_type_id, value) VALUES (6, 3, 'Month');
INSERT INTO list_values (list_value_id, list_value_type_id, value) VALUES (7, 4, 'AMD');
INSERT INTO list_values (list_value_id, list_value_type_id, value) VALUES (8, 4, 'INTEL');


--
-- TOC entry 2192 (class 0 OID 41973)
-- Dependencies: 191
-- Data for Name: object_references; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO object_references (ref_id, object_id, object_ref_id, ref_order, ref_attribute) VALUES (1, 1, 2, 2, 1);


--
-- TOC entry 2187 (class 0 OID 33632)
-- Dependencies: 186
-- Data for Name: objects; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (1, 2, 'Monitor', 1, 'Structurally complete device for displaying visual information.', 1, 1);
INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (2, 4, 'Food processor', 2, 'A kitchen appliance used to facilitate repetitive tasks in the preparation of food.', 2, 2);
INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (3, 1, 'Smartphone', 3, 'A mobile phone with an advanced mobile operating system which combines features of a personal computer operating system with other features useful for mobile or handheld use.', 3, 3);
INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (4, 2, 'Keyboard', 4, 'A typewriter-style device which uses an arrangement of buttons or keys to act as a mechanical lever or electronic switch.', 4, 1);
INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (5, 4, 'Juicer', 5, ' A tool for separating juice from fruits, herbs, leafy greens and other types of vegetables from its pulp in a process called juicing. ', 5, 4);
INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (6, 3, 'Xbox', 6, 'A video gaming brand created and owned by Microsoft.', 6, 5);
INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (7, 12, 'UniversalParent', 7, 'Just workaround ', 7, 7);
INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (57, 8, 'Dmitry', 7, 'user ', 7, 1);
INSERT INTO objects (object_id, type_id, name, parent_id, description, project_id, order_id) VALUES (71, 8, 'aaa', 7, 'user ', 7, 1);


--
-- TOC entry 2205 (class 0 OID 0)
-- Dependencies: 190
-- Name: reference_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('reference_seq', 1, false);


--
-- TOC entry 2206 (class 0 OID 0)
-- Dependencies: 189
-- Name: type_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('type_seq', 30, true);


--
-- TOC entry 2183 (class 0 OID 33586)
-- Dependencies: 182
-- Data for Name: types; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO types (type_id, name, description, properties) VALUES (1, 'Phones', 'Telecommunications device that permits two or more users to conduct a conversation when they are too far apart to be heard directly.', NULL);
INSERT INTO types (type_id, name, description, properties) VALUES (2, 'Laptops, tablets and PCs', 'General purpose device that can be programmed to carry out a set of arithmetic or logical operations automatically.', NULL);
INSERT INTO types (type_id, name, description, properties) VALUES (3, 'TV and Video', 'elevision is a telecommunication medium used for transmitting sound with moving images in monochrome (black-and-white), or in color, and in two or three dimensions.', NULL);
INSERT INTO types (type_id, name, description, properties) VALUES (4, 'Home Appliances', 'Home appliances are electrical/mechanical machines which accomplish some household functions, such as cooking or cleaning. ', NULL);
INSERT INTO types (type_id, name, description, properties) VALUES (28, 'My Cystom Type', 'test', 'key=null;');
INSERT INTO types (type_id, name, description, properties) VALUES (8, 'User', 'User entity type', '');
INSERT INTO types (type_id, name, description, properties) VALUES (12, 'EmptyType', 'Just workaround', '');


--
-- TOC entry 2193 (class 0 OID 42024)
-- Dependencies: 192
-- Data for Name: values; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO "values" (value_id, object_id, attribute_id, value_int, value_string, value_date, value_listvalue_id) VALUES (1, 1, 1, NULL, 'red', NULL, NULL);
INSERT INTO "values" (value_id, object_id, attribute_id, value_int, value_string, value_date, value_listvalue_id) VALUES (2, 1, 2, 56, NULL, NULL, NULL);
INSERT INTO "values" (value_id, object_id, attribute_id, value_int, value_string, value_date, value_listvalue_id) VALUES (3, 1, 4, NULL, NULL, NULL, 8);
INSERT INTO "values" (value_id, object_id, attribute_id, value_int, value_string, value_date, value_listvalue_id) VALUES (58, 57, 6, NULL, 'Dmitry', NULL, NULL);
INSERT INTO "values" (value_id, object_id, attribute_id, value_int, value_string, value_date, value_listvalue_id) VALUES (59, 57, 7, NULL, '2a7e6103d0410db3025a72e2f31ef5ff', NULL, NULL);
INSERT INTO "values" (value_id, object_id, attribute_id, value_int, value_string, value_date, value_listvalue_id) VALUES (72, 71, 6, NULL, 'aaa', NULL, NULL);
INSERT INTO "values" (value_id, object_id, attribute_id, value_int, value_string, value_date, value_listvalue_id) VALUES (73, 71, 7, NULL, '2197bb99ec05d4b287f4c568305e3c84', NULL, NULL);


--
-- TOC entry 2039 (class 2606 OID 33606)
-- Name: attr_list_value_attribute_id_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attr_list_value
    ADD CONSTRAINT attr_list_value_attribute_id_pk PRIMARY KEY (attribute_id);


--
-- TOC entry 2041 (class 2606 OID 33714)
-- Name: attr_types_attribute_id_type_id_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attr_types
    ADD CONSTRAINT attr_types_attribute_id_type_id_pk PRIMARY KEY (attribute_id, type_id);


--
-- TOC entry 2037 (class 2606 OID 33601)
-- Name: attributes_attribute_id_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attributes
    ADD CONSTRAINT attributes_attribute_id_pk PRIMARY KEY (attribute_id);


--
-- TOC entry 2045 (class 2606 OID 33705)
-- Name: data_type_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY data_types
    ADD CONSTRAINT data_type_id PRIMARY KEY (id);


--
-- TOC entry 2033 (class 2606 OID 33580)
-- Name: list_value_list_value_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY list_values
    ADD CONSTRAINT list_value_list_value_id PRIMARY KEY (list_value_id);


--
-- TOC entry 2031 (class 2606 OID 33572)
-- Name: list_value_type_list_value_type_id_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY list_value_type
    ADD CONSTRAINT list_value_type_list_value_type_id_pk PRIMARY KEY (list_value_type_id);


--
-- TOC entry 2047 (class 2606 OID 41977)
-- Name: object_references_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT object_references_pkey PRIMARY KEY (ref_id);


--
-- TOC entry 2043 (class 2606 OID 33639)
-- Name: objects_object_id_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY objects
    ADD CONSTRAINT objects_object_id_pk PRIMARY KEY (object_id);


--
-- TOC entry 2035 (class 2606 OID 33593)
-- Name: types_attribute_id_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_attribute_id_pk PRIMARY KEY (type_id);


--
-- TOC entry 2051 (class 2606 OID 42291)
-- Name: value_id_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "values"
    ADD CONSTRAINT value_id_pk PRIMARY KEY (value_id);


--
-- TOC entry 2049 (class 1259 OID 42630)
-- Name: fki_values_listvalues_fk; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_values_listvalues_fk ON "values" USING btree (value_listvalue_id);


--
-- TOC entry 2048 (class 1259 OID 41993)
-- Name: object_references_ref_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX object_references_ref_id_uindex ON object_references USING btree (ref_id);


--
-- TOC entry 2054 (class 2606 OID 42691)
-- Name: attr_list_value_attribute_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attr_list_value
    ADD CONSTRAINT attr_list_value_attribute_id_fk FOREIGN KEY (attribute_id) REFERENCES attributes(attribute_id) ON DELETE CASCADE;


--
-- TOC entry 2055 (class 2606 OID 42696)
-- Name: attr_list_value_list_value_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attr_list_value
    ADD CONSTRAINT attr_list_value_list_value_type_id_fk FOREIGN KEY (list_value_type_id) REFERENCES list_value_type(list_value_type_id) ON DELETE CASCADE;


--
-- TOC entry 2056 (class 2606 OID 42681)
-- Name: attr_types_attribute_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attr_types
    ADD CONSTRAINT attr_types_attribute_fk FOREIGN KEY (attribute_id) REFERENCES attributes(attribute_id) ON DELETE CASCADE;


--
-- TOC entry 2057 (class 2606 OID 42686)
-- Name: attr_types_type_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attr_types
    ADD CONSTRAINT attr_types_type_fk FOREIGN KEY (type_id) REFERENCES types(type_id) ON DELETE CASCADE;


--
-- TOC entry 2063 (class 2606 OID 42721)
-- Name: attribute_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT attribute_fk FOREIGN KEY (ref_attribute) REFERENCES attributes(attribute_id) ON DELETE CASCADE;


--
-- TOC entry 2053 (class 2606 OID 42706)
-- Name: attributes_datatype_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attributes
    ADD CONSTRAINT attributes_datatype_fk FOREIGN KEY (atr_type) REFERENCES data_types(id) ON DELETE CASCADE;


--
-- TOC entry 2052 (class 2606 OID 42701)
-- Name: list_value_type_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY list_values
    ADD CONSTRAINT list_value_type_fk FOREIGN KEY (list_value_type_id) REFERENCES list_value_type(list_value_type_id) ON DELETE CASCADE;


--
-- TOC entry 2061 (class 2606 OID 42711)
-- Name: object_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT object_fk FOREIGN KEY (object_id) REFERENCES objects(object_id) ON DELETE CASCADE;


--
-- TOC entry 2062 (class 2606 OID 42716)
-- Name: object_ref_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_references
    ADD CONSTRAINT object_ref_fk FOREIGN KEY (object_ref_id) REFERENCES objects(object_id) ON DELETE CASCADE;


--
-- TOC entry 2059 (class 2606 OID 33645)
-- Name: objects_parent_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY objects
    ADD CONSTRAINT objects_parent_id_fk FOREIGN KEY (parent_id) REFERENCES objects(object_id);


--
-- TOC entry 2060 (class 2606 OID 33650)
-- Name: objects_project_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY objects
    ADD CONSTRAINT objects_project_id_fk FOREIGN KEY (project_id) REFERENCES objects(object_id);


--
-- TOC entry 2058 (class 2606 OID 33640)
-- Name: objects_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY objects
    ADD CONSTRAINT objects_type_id_fk FOREIGN KEY (type_id) REFERENCES types(type_id);


--
-- TOC entry 2065 (class 2606 OID 42731)
-- Name: values_attributes_attribute_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "values"
    ADD CONSTRAINT values_attributes_attribute_id_fk FOREIGN KEY (attribute_id) REFERENCES attributes(attribute_id) ON DELETE CASCADE;


--
-- TOC entry 2066 (class 2606 OID 42736)
-- Name: values_listvalues_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "values"
    ADD CONSTRAINT values_listvalues_fk FOREIGN KEY (value_listvalue_id) REFERENCES list_values(list_value_id);


--
-- TOC entry 2064 (class 2606 OID 42726)
-- Name: values_objects__fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "values"
    ADD CONSTRAINT values_objects__fk FOREIGN KEY (object_id) REFERENCES objects(object_id) ON DELETE CASCADE;


--
-- TOC entry 2201 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-05-20 17:00:14

--
-- PostgreSQL database dump complete
--

