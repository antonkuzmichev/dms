ALTER TABLE types
   ADD COLUMN properties character varying;

ALTER TABLE attributes
   ADD COLUMN properties character varying;
