
CREATE TABLE public.list_value_type (
                list_value_type_id BIGINT NOT NULL,
                name VARCHAR NOT NULL,
                CONSTRAINT list_value_type_list_value_type_id_pk PRIMARY KEY (list_value_type_id)
);


CREATE TABLE public.list_value (
                list_value_id BIGINT NOT NULL,
                list_value_type_id BIGINT NOT NULL,
                value VARCHAR NOT NULL,
                CONSTRAINT list_value_list_value_id PRIMARY KEY (list_value_id),
			CONSTRAINT list_value_list_value_type_id_fk
			  FOREIGN KEY (list_value_type_id)
			  REFERENCES public.list_value_type (list_value_type_id)

);


CREATE TABLE public.types (
                type_id BIGINT NOT NULL,
                name VARCHAR NOT NULL,
                description VARCHAR NOT NULL,
                CONSTRAINT types_attribute_id_pk PRIMARY KEY (type_id)
);


CREATE TABLE public.attributes (
                attribute_id BIGINT NOT NULL,
                name VARCHAR NOT NULL,
                atr_type INTEGER NOT NULL,
                description VARCHAR NOT NULL,
                CONSTRAINT attributes_attribute_id_pk PRIMARY KEY (attribute_id)
);


CREATE TABLE public.attr_list_value (
                attribute_id BIGINT NOT NULL,
                list_value_type_id BIGINT NOT NULL,
                CONSTRAINT attr_list_value_attribute_id_pk PRIMARY KEY (attribute_id),
			CONSTRAINT attr_list_value_list_value_type_id_fk
			  FOREIGN KEY (list_value_type_id)
			  REFERENCES public.list_value_type (list_value_type_id),
			CONSTRAINT attr_list_value_attribute_id_fk
			  FOREIGN KEY (attribute_id)
			  REFERENCES public.attributes (attribute_id)


);


CREATE TABLE public.attr_types (
                attribute_id BIGINT NOT NULL,
                type_id BIGINT NOT NULL,
                CONSTRAINT attr_types_attribute_id_pk PRIMARY KEY (attribute_id),
			CONSTRAINT attr_types_type_id_fk
			  FOREIGN KEY (type_id)
			  REFERENCES public.types (type_id),
			CONSTRAINT attr_types_attribute_id_fk
			  FOREIGN KEY (attribute_id)
			  REFERENCES public.attributes (attribute_id)

);


CREATE TABLE public.objects (
                object_id BIGINT NOT NULL,
                type_id BIGINT NOT NULL,
                name VARCHAR NOT NULL,
                parent_id BIGINT NOT NULL,
                description VARCHAR NOT NULL,
                project_id BIGINT NOT NULL,
                order_id BIGINT NOT NULL,
                CONSTRAINT objects_object_id_pk PRIMARY KEY (object_id),
			CONSTRAINT objects_type_id_fk
			  FOREIGN KEY (type_id)
			  REFERENCES public.types (type_id),
			CONSTRAINT objects_parent_id_fk
			  FOREIGN KEY (parent_id)
			  REFERENCES public.objects (object_id),
			CONSTRAINT objects_project_id_fk
			  FOREIGN KEY (project_id)
			  REFERENCES public.objects (object_id)

);


CREATE TABLE public.values (
                object_id BIGINT NOT NULL,
                list_value_id BIGINT NOT NULL,
                attribute_id BIGINT NOT NULL,
                value VARCHAR NOT NULL,
                date DATE NOT NULL,
                CONSTRAINT values_object_id_pk PRIMARY KEY (object_id),
			CONSTRAINT values_list_value_id_fk
			  FOREIGN KEY (list_value_id)
			  REFERENCES public.list_value (list_value_id),
			CONSTRAINT values_attribute_id_fk
			  FOREIGN KEY (attribute_id)
			  REFERENCES public.attributes (attribute_id),
			CONSTRAINT values_object_id_fk
			  FOREIGN KEY (object_id)
			  REFERENCES public.objects (object_id)


);


CREATE TABLE public.references (
                object_id BIGINT NOT NULL,
                reference BIGINT NOT NULL,
                attribute_id BIGINT NOT NULL,
                order_1 BIGINT NOT NULL,
                CONSTRAINT references_object_id_pk PRIMARY KEY (object_id),
			CONSTRAINT references_attribute_id_fk
				FOREIGN KEY (attribute_id)
				REFERENCES public.attributes (attribute_id),
			CONSTRAINT references_object_id_fk
			  FOREIGN KEY (object_id)
			  REFERENCES public.objects (object_id),
			CONSTRAINT references_reference_fk
			  FOREIGN KEY (reference)
			  REFERENCES public.objects (object_id)

);