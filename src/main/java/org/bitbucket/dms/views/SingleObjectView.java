package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.dao.ReferenceDAO;
import org.bitbucket.dms.dao.ValueDAO;
import org.bitbucket.dms.model.ObjectModel;
import org.bitbucket.dms.model.Reference;
import org.bitbucket.dms.model.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;

@ViewScoped
@ManagedBean
public class SingleObjectView {

    private ObjectModel object;


    private Long id;
//    private String name;
//    private String description;
//    private Type type;
//    private ObjectModel parent;
//    private ObjectModel project;
//    private Long order;

    @EJB
    private GenericDAO<ObjectModel, Long> objectDAO;
    @EJB
    private ReferenceDAO referenceDAO;
    @EJB
    private ValueDAO valueDAO;

    private List<Reference> references;
    private Reference selectedReference;
    private List<Value> values;
    private Value selectedValue;
    public ObjectModel getObject(){ return object; }
    Logger log = LoggerFactory.getLogger(SingleObjectView.class);

    private void initFields(){
//        name = object.getName();
//        description = object.getDescription();
//        type = object.getType();
//        parent = object.getParent();
//        project = object.getProjectID();
//        order = object.getOrderID();
    }

//    @PostConstruct
    public void init() {
        log.info("SingleObject_view initialization");
        log.info("Id = " + id);

        object = objectDAO.getById(ObjectModel.class, id);
        if(object == null) {
            log.warn("ID not found!!!");
            return;
        }
        references = referenceDAO.getByObjectId(id);
        values = valueDAO.getByObjectId(id);

        initFields();
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public List<Reference> getReferences() {
        return references;
    }
    public Reference getSelectedReference() {
        return selectedReference;
    }

    public List<Value> getValues() {return values;}

    public Value getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(Value selectedValue) {
        this.selectedValue = selectedValue;
    }

    public void setSelectedReference(Reference selectedReference) {
        this.selectedReference = selectedReference;
    }
    public String deleteObject()
    {

        HttpSession session = SessionBean.getSession();
        if (object.getId()==session.getAttribute("userId")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Delete yourself must you not!", "(C) Yoda, long time ago"));
            return null;
        } else   objectDAO.delete(object);
        return "objects_view?faces-redirect=true";
    }
    //
//    public String getName() { return name; }
//    public void setName(String name) { this.name = name; }
//
//    public Type getType() { return type; }
//    public void setType(Type type) { this.type = type; }
//
//    public String getDescription() { return description; }
//    public void setDescription(String description) { this.description = description; }
//
//    public ObjectModel getParent() { return parent; }
//    public void setParent(ObjectModel parent) { this.parent = parent; }
//
//    public ObjectModel getProject() { return  project; }
//    public void setProject(ObjectModel project) { this.project = project; }
//
//    public Long getOrder() { return order; }
//    public void setOrder(Long order) { this.order = order; }

}
