package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.ReferenceDAO;
import org.bitbucket.dms.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;


@ViewScoped
@ManagedBean
public class AllReferencesView implements Serializable{

    protected static final Logger logger =
            LoggerFactory.getLogger(AllReferencesView.class.getName());
    protected List <Reference> references;
    protected Reference selectedReference;
    @EJB
    protected ReferenceDAO referenceDAO;

    @PostConstruct
    public void init() {

        references = referenceDAO.getAll(Reference.class);
        logger.info("References list created");


    }

    public List<Reference> getReferences() {
        return references;
    }
    public Reference getSelectedReference() {
        return selectedReference;
    }

    public void setSelectedReference(Reference selectedReference) {
        logger.info("reference is selected");
        this.selectedReference = selectedReference;
    }

    public void deleteReference() {
        referenceDAO.delete(selectedReference);
        references.remove(selectedReference);
        selectedReference = null;
    }

    public String editReference() {
        return"reference_edit?faces-redirect=true id="+selectedReference.getId();
    }







}