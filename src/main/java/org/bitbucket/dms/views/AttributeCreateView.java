package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.DataType;
import org.bitbucket.dms.model.ListValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Author: Popov Dmitry
 */


@ViewScoped
@ManagedBean
public class AttributeCreateView implements Serializable{

    private static Logger logger =
            LoggerFactory.getLogger(AttributeCreateView.class.getName());
    private Attribute attribute = new Attribute();
    @EJB
    private GenericDAO<Attribute, Long> attributeDAO;
    @EJB
    private GenericDAO<DataType, Long> dataTypeDAO;
    @EJB
    private GenericDAO dao;
    private List<ListValueType> listValueTypeList;
    private List<DataType> dataTypeList;


    @PostConstruct
    public void init() {
        dataTypeList = dataTypeDAO.getAll(DataType.class);
        logger.debug("Data types list created");
        listValueTypeList = dao.getAll(ListValueType.class);
        logger.debug("ListValueType list created");
    }


    public List<ListValueType> getListValueTypeList() {
        return listValueTypeList;
    }

    public List<DataType> getDataTypeList() {return dataTypeList;}

    public AttributeCreateView() {
    }


    public boolean isList() {
        if(attribute.getDataType()==null) return false;
        return (attribute.getDataType().getId() == 4L);
    }

    public String addAttribute() {
        attributeDAO.add(attribute);
        return "attribute_view?faces-redirect=true";
    }

    public Attribute getAttribute() {
        return attribute;
    }
}
