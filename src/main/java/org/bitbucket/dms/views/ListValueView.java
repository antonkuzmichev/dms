package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.ListValue;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import java.util.List;

@ViewScoped
@ManagedBean
public class ListValueView {
     private List <ListValue> listValues;

     private ListValue selectedListValue;
    @EJB
    private GenericDAO<ListValue, Long> listValuesDAO;

    @PostConstruct
    public void init() {
    	listValues = listValuesDAO.getAll(ListValue.class);
    }

    public List<ListValue> getListValues() {
        return listValues;
    }

    public ListValue getSelectedListValue() {
        return selectedListValue;
    }

    public void setSelectedListValue(ListValue selectedListValue) {
        this.selectedListValue = selectedListValue;
    }
    public void delete() {
        addMessage("Success", "Data deleted");
    }
    
    public void deleteListValue() {
        listValuesDAO.delete(selectedListValue);
        selectedListValue = null;
    }
    public void edit() {
        addMessage("Success", "Data edited");
    }
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    
}
