package org.bitbucket.dms.views.converters;


import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.DataType;
import org.bitbucket.dms.model.DataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "dataTypeConverter")
@ManagedBean
@ViewScoped
@Stateless
public class DataTypeConverter implements Converter {


    private static Logger logger =
            LoggerFactory.getLogger(DataTypeConverter.class.getName());
    @EJB
    private GenericDAO<DataType,Long> dataTypeDAO;


    public DataType getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null) {
            try {

                if(dataTypeDAO==null) logger.info("dao is NULL!!!! ");
                logger.info("value="+value);
                return (DataType) dataTypeDAO.getById(DataType.class, Long.valueOf(value));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid attr."));
            }
        }
        else {
            return null;
        }
    }

    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            return String.valueOf(((DataType) object).getId());
        }
        else {
            return null;
        }
    }


}