package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Doc;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;


/**
 * Author: Popov Dmitry
 */


@ViewScoped
@ManagedBean
public class DocumentsView implements Serializable{

    private static final Logger logger =
            LoggerFactory.getLogger(DocumentsView.class.getName());
    private List <Doc> docs;
    @EJB
    private GenericDAO dao;

    @PostConstruct
    public void init() {

        docs=dao.getAll(Doc.class);
        logger.info("Docs list created");

    }

    public List<Doc> getDocs() {
        return docs;
    }
}