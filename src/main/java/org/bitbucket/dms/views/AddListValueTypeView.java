package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.ListValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ViewScoped
@ManagedBean
public class AddListValueTypeView {

    private static Logger logger =
            LoggerFactory.getLogger(AddListValueTypeView.class.getName());
    private ListValueType listValueType =new ListValueType();
    @EJB
    private GenericDAO<ListValueType,Long> listValueTypeDAO;

    public AddListValueTypeView() {

    }

    public String addListValueType () {
    	listValueTypeDAO.add(listValueType);
        return "list_value?faces-redirect=true";
    }
    public ListValueType getListValueType (){
        return listValueType;
    }
    public String cancel () {
        return "list_value?faces-redirect=true";
    }
}
