package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;


/**
 * Author: Popov Dmitry
 */


@ManagedBean
@ViewScoped
public class TypeCreateView {

    private static Logger logger =
            LoggerFactory.getLogger(TypeCreateView.class.getName());
    private final Type type=new Type();
    private List<Attribute> attributes;
    @EJB
    private GenericDAO<Type,Long> typeDAO;
    @EJB
    private GenericDAO<Attribute,Long> attributeDAO;


    @PostConstruct
    public void init() {
        attributes=attributeDAO.getAll(Attribute.class);
    }

    public TypeCreateView() {

    }
    public List<Attribute> getAttributes() {
        return attributes;
    }

    public String addType () {
        typeDAO.add(type);

        return "types_view?faces-redirect=true";
    }
    public Type getType (){
        return type;
    }
}
