package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.ListValueTypeDAO;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.ListValue;
import org.bitbucket.dms.model.ListValueType;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import java.io.Serializable;
import java.util.List;

@ViewScoped
@ManagedBean
public class ListValueTypeView implements Serializable{
	
     private List <ListValueType> listValueTypes;
     private ListValueType selectedListValueType;
    @EJB
    private ListValueTypeDAO listValueTypeDAO;
    @EJB
    private GenericDAO<ListValue, Long> listValuesDAO;
    @PostConstruct
    public void init() {
    	listValueTypes = listValueTypeDAO.getAll();
    }
    
    public List<ListValueType> getListValueTypes() {
        return listValueTypes;
    }
    public ListValueType getSelectedListValueType() {
        return selectedListValueType;
    }

    public void setSelectedListValueType(ListValueType selectedListValueType) {
        this.selectedListValueType = selectedListValueType;
    }
    
    public void deleteListValueType() {
        listValueTypeDAO.delete(selectedListValueType);
        selectedListValueType = null;
        addMessage("Success", "Data deleted");
    }
    public String editListValueType() {
        return"edit_list_value_type?faces-redirect=true listValueTypeId="+selectedListValueType.getId();
    }
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    
}
