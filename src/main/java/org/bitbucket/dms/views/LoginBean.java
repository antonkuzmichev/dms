package org.bitbucket.dms.views;

import org.bitbucket.dms.model.ObjectModel;
import org.bitbucket.dms.utils.UserUtils;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.validation.ValidationException;
import java.io.Serializable;

@ManagedBean
@SessionScoped

public class LoginBean implements Serializable{

    @EJB
    private UserUtils userUtils;

    private boolean logged = false;
    private String login;
    private String password;
    private ObjectModel userObject;

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ObjectModel getUserObject() {
        return userObject;
    }

    public String doLogin(){
        if(userUtils.checkUserAuth(login,password)){
        	HttpSession session = SessionBean.getSession();
        	session.setAttribute("username", login);
            session.setAttribute("userId", userUtils.getUserObject(login).getId());
        	return "index";
        } else
        	return "loginfailed";
   }
   
    public String logout() {
        HttpSession session = SessionBean.getSession();
        session.invalidate();
        return "home";
    }
/*
    public void validateName(FacesContext context, UIComponent component,
                             Object value){

        String name = (String) value;
        if(name == null || name.length() < 3){
            context.addMessage(null, new FacesMessage("�������� ��� ������������"));
            throw new ValidationException();
        }
    }

    public void validatePassword(FacesContext context, UIComponent component,
                                 Object value){
        String login = (String)value;
        if(login == null || login.length() < 3){
            context.addMessage(null, new FacesMessage("�������� ������"));
            throw new ValidationException();
        }
    }
    public String checkUser(){
        if(this.login.equals("user") && this.password.equals("123")){
            return "index";
        }else{
            return "loginfailed";
        }
    }*/
}
