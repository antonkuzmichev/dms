package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.ListValueTypeDAO;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.ListValue;
import org.bitbucket.dms.model.ListValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import java.util.List;
import java.util.ListIterator;

@ViewScoped
@ManagedBean
public class EditListValueTypeView {

    private ListValueType listValueType;
    private List<ListValue> listValues;
    private Long listValueTypeId;
    private String name;
    private String listValue;

    
    @EJB
    private ListValueTypeDAO listValueTypeDAO;
    @EJB
    private GenericDAO<ListValue, Long> listValueDAO;

    public ListValueType getListValueType() { 
    	return listValueType; 
    }
    
    public void completeText(List<ListValue> listValue) {
    	ListIterator<ListValue> itr = listValue.listIterator();
    	while (itr.hasNext()){
    		for(int i = 0; i < listValue.size(); i++) {
                listValues.add((ListValue) listValue);
            }
    	}
         
    }
    
    private void initFields() {
    	
        name = listValueType.getName();
        listValues = listValueType.getListValues();
    }

    public void init() {
        Logger log = LoggerFactory.getLogger(EditListValueTypeView.class);
        log.info("List value type edit view initialization");
        log.info("Id = " + listValueTypeId);

        listValueType = listValueTypeDAO.getByListValueTypeId(listValueTypeId);
        if (listValueType == null) {
            log.warn("ID not found!!!");
            return;
        }

        initFields();

    }

    public Long getListValueTypeId() {
    	return listValueTypeId; 
    }
    public void setListValueTypeId(Long listValueTypeId) {
    	this.listValueTypeId = listValueTypeId; 
    }
    
    public String getName() {
    	return name; 
    }
    public void setName(String name) {
    	this.name = name; 
    }
    
    public String getListValue() {
    	return listValue; 
    }
    public void setListValue(String listValue) {
    	this.listValue = listValue; 
    }
    
    public String update () {
        listValueTypeDAO.update(listValueType);
        return "list_value?faces-redirect=true";
    }
    public String addNewValue () {
    	if(!StringUtils.isEmpty(getListValue())) {
    		ListValue lv = new ListValue();
    		lv.setValue(getListValue());
    		lv.setListValueType(listValueTypeDAO.getByListValueTypeId(this.getListValueTypeId()));
    		this.getListValueType().getListValues().add(lv);
    		this.listValueDAO.update(lv);
    	}
    	listValueTypeDAO.update(listValueType);
    	return "list_value?faces-redirect=true";
    }
    public String cancel () {
        return "list_value?faces-redirect=true";
    }

}
