package org.bitbucket.dms.views;
import java.io.*;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@ManagedBean
@SessionScoped
public class FileUploadView implements Serializable {

    private static final long serialVersionUID = 1L;

    Logger log = LoggerFactory.getLogger(FileUploadView.class);
    private String name;
    private UploadedFile uploadedFile;
    private String filePath;
    private String tempFileName="UploadedFile.docx";
    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }
    public boolean uploadCompleted;
    private String fileName;
    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public boolean isUploadCompleted() {
        return uploadCompleted;
    }

    public void setUploadCompleted(boolean uploadCompleted) {
        this.uploadCompleted = uploadCompleted;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getFileName() {
        return fileName;
    }
    private void setFileName(String fileName)
    {
        this.fileName = fileName;
    }


    public void uploadFile(FileUploadEvent e) {

        uploadedFile=e.getFile();
        String folder="c:/to_parse/";

        byte[] bytes=null;
        if (null!=uploadedFile) {
            bytes = uploadedFile.getContents();
            fileName =FilenameUtils.getName(uploadedFile.getFileName());
            filePath=folder+ fileName;
            BufferedOutputStream stream = null;
            try {
                stream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
                stream.write(bytes);
                stream.close();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            uploadCompleted =true;
            log.info("File uploaded");
            log.info("filepath = "+ filePath+ "fileName = "+ fileName);
        }
        FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,"File "+ fileName + "  uploaded successfully", ""));

        //FacesContext.getCurrentInstance().getExternalContext().redirect("successful_upload.xhtml");
    }
    public String parse()
    {
        return "changes_preview?faces-redirect=true&filename="+fileName;
    }
}
