package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Author: Popov Dmitry
 */



@ManagedBean
@ViewScoped
public class TypeEditView {

    private Type type;
    private Long id;
    private List<Attribute> attributes;
    private List<Attribute> selectedAttributes;
    @EJB
    private GenericDAO<Type,Long> typeDAO;
    @EJB
    private GenericDAO<Attribute,Long> attributeDAO;



    public void init() {
        Logger log = LoggerFactory.getLogger(TypeEditView.class);
        log.info("Type edit view initialization");
        log.info("Id = " + id);
        attributes=attributeDAO.getAll(Attribute.class);
        type = typeDAO.getById(Type.class, id);
        selectedAttributes =type.getAttributes();
        if (type == null) {
            log.warn("ID not found!!!");
        }

    }

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}
    public Type getType() {return type;}
    public void setType(Type type) {this.type = type;}

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public List<Attribute> getSelectedAttributes() {
        return selectedAttributes;
    }

    public void setSelectedAttributes(List<Attribute> selectedAttributes) {
        this.selectedAttributes = selectedAttributes;
    }

    public void update ()
    {
        type.setAttributes(selectedAttributes);
        typeDAO.update(type);
    }
    public void cancel ()
    {
    }

}
