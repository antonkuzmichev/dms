package org.bitbucket.dms.views;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Attribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ViewScoped
@ManagedBean
public class AttributeView implements Serializable{

    private static final Logger logger =
            LoggerFactory.getLogger(AttributeView.class.getName());
    private List <Attribute> attributes;
    private List <Attribute> filteredAttributes;
    private Attribute selectedAttribute;
    @EJB
    private GenericDAO<Attribute,Long> attributeDAO;

    @PostConstruct
    public void init() {

        attributes = attributeDAO.getAll(Attribute.class);
        logger.info("Attributes list created");


    }

    public List<Attribute> getAttributes() {
        return attributes;
    }
    public Attribute getSelectedAttribute() {
        return selectedAttribute;
    }

    public void setSelectedAttribute(Attribute selectedAttribute) {
        logger.info("attribute is selected");
        this.selectedAttribute = selectedAttribute;
    }

    public List<Attribute> getFilteredAttributes() {
        return filteredAttributes;
    }

    public void setFilteredAttributes(List<Attribute> filteredAttributes) {
        this.filteredAttributes = filteredAttributes;
    }

    public void deleteAttribute() {
        attributeDAO.delete(selectedAttribute);
        attributes.remove(selectedAttribute);
        selectedAttribute = null;
    }

    public String editAttribute() {
        return "attribute_edit?faces-redirect=true id="+selectedAttribute.getId();
    }
}