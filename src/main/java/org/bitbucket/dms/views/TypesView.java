package org.bitbucket.dms.views;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;


/**
 * Author: Popov Dmitry
 */


@ViewScoped
@ManagedBean
public class TypesView implements Serializable{

    private static final Logger logger =
            LoggerFactory.getLogger(TypesView.class.getName());
    private List <Type> types;
    private Type selectedType;
    private List <Type> filteredTypes;

    @EJB
    private GenericDAO<Type,Long> typeDAO;

    @PostConstruct
    public void init() {

        types = typeDAO.getAll(Type.class);
        logger.info("Types list created");

    }

    public List<Type> getTypes() {
        return types;
    }
    public Type getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(Type selectedType) {
        logger.info("type is selected");
        this.selectedType = selectedType;
    }

    public List<Type> getFilteredTypes() {
        return filteredTypes;
    }

    public void setFilteredTypes(List<Type> filteredTypes) {
        this.filteredTypes = filteredTypes;
    }

    public void deleteType() {
        typeDAO.delete(selectedType);
        types.remove(selectedType);
        selectedType = null;
    }



    public String editType() {
        return"type_edit?faces-redirect=true id="+selectedType.getId();
    }



}