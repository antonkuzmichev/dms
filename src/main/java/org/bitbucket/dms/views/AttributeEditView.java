package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.DataType;
import org.bitbucket.dms.model.ListValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Popov Dmitry
 */

@ViewScoped
@ManagedBean
public class AttributeEditView {

    private Attribute attribute;
    private Long id;

    @EJB
    private GenericDAO<Attribute,Long> attributeDAO;

    @EJB
    private GenericDAO<DataType, Long> dataTypeDAO;

    @EJB
    private GenericDAO<ListValueType, Long> listValueTypeDAO;

    private List<ListValueType> listValueTypeList;

    private final Map<String, Long> typesMap=new HashMap<>();


    public void init() {
        Logger log = LoggerFactory.getLogger(AttributeEditView.class);
        log.info("Attribute edit view initialization");
        log.info("Id = " + id);

        attribute = attributeDAO.getById(Attribute.class, id);
        if (attribute == null) {
            log.warn("ID not found!!!");
            return;
        }

        List<DataType> dataTypeList = dataTypeDAO.getAll(DataType.class);
        log.info("Data types list created");
        listValueTypeList = listValueTypeDAO.getAll(ListValueType.class);
        for (DataType dataType: dataTypeList) {
            typesMap.put(dataType.getName(),dataType.getId());
        }

    }
    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public Attribute getAttribute() {return attribute;}
    public void setAttribute(Attribute attribute) {this.attribute = attribute;}

    public List<ListValueType> getListValueTypeList() {
        return listValueTypeList;
    }

    public void update ()
    {
        attributeDAO.update(attribute);
    }
    public void cancel ()    {    }
    public boolean isList() {
        if(attribute.getDataType()==null) return false;
        return (attribute.getDataType().getId() == 4L);
    }

}
