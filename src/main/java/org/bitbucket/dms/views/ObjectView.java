package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.ObjectModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ViewScoped
@ManagedBean
public class ObjectView {

    private List<ObjectModel> objects;
    private List<ObjectModel> filteredObjects;

    @EJB
    private GenericDAO<ObjectModel, Long> objectDAO;

    public static Logger Log = LoggerFactory.getLogger(ObjectView.class);

    @PostConstruct
    public void init() {

        Log.info("Object_view initialization");
        objects = objectDAO.getAll(ObjectModel.class);

    }

    public List<ObjectModel> getObjects() {
        return objects;
    }

    public List<ObjectModel> getFilteredObjects() {
        return filteredObjects;
    }

    public void setFilteredObjects(List<ObjectModel> filteredObjects) {
        this.filteredObjects = filteredObjects;
    }
}
