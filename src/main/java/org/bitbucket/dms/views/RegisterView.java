package org.bitbucket.dms.views;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.bitbucket.dms.utils.UserUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ViewScoped
@ManagedBean
@Stateful
public class RegisterView {

    Logger log = LoggerFactory.getLogger(RegisterView.class);

    @EJB
    private UserUtils userUtils;
    private boolean logged = false;
    private String login;
    private String password;

    public RegisterView() {
        log.info("RegisterView constructed");
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLogged() {
        return logged;
    }

    public void setLogged(boolean logged) {
        this.logged = logged;
    }

    public String addUser() {
        if (userUtils.userIsExist(login)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Username already exists"));
            return null;
        } else {
            userUtils.registerUser(login, password);
            HttpSession session = SessionBean.getSession();
            session.setAttribute("username", login);
            return "index?faces-redirect=true";
        }

    }

    public String cancel() {
        return "home";
    }


}
