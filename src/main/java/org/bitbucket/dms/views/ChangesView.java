package org.bitbucket.dms.views;

import com.sun.org.apache.regexp.internal.RE;
import com.sun.webkit.graphics.Ref;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.docxParser.MultiParser;
import org.bitbucket.dms.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;


@ViewScoped
@ManagedBean
public class ChangesView implements Serializable{
    private static Logger Log = LoggerFactory.getLogger(ChangesView.class);

    public class ProxyGenericEntity<E>{
        int style;
        E entity;

        public E getEntity(){
            return entity;
        }
        public void setEntity(E entity){
            this.entity = entity;
        }

        ProxyGenericEntity(int style, E entity){
            this.style = style;
            this.entity = entity;
        }

        public String getStyle(){
            switch (style){
                case 1:
                    return "newEntity";
                case 2:
                    return  "updatedEntity";
                case 3:
                    return "deletedEntity";
                default:
                    Log.warn("Unexpected proxyEntity style");
                    return null;
            }
        }
    }

    @EJB
    GenericDAO DAO;

    List<ProxyGenericEntity<GenericEntity>> entities;

    List<ProxyGenericEntity<Type>> types;
    List<ProxyGenericEntity<Attribute>> attributes;
    List<ProxyGenericEntity<ListValueType>> listTypes;
    List<ProxyGenericEntity<ListValue>> listValues;
    List<ProxyGenericEntity<ObjectModel>> objects;
    List<ProxyGenericEntity<Value>> values;
    List<ProxyGenericEntity<Reference>> references;

    private boolean isTypeRendered;
    private boolean isAttributeRendered;
    private boolean isListsRendered;
    private boolean isListValuesRendered;
    private boolean isObjectsRendered;
    private boolean isValuesRendered;
    private boolean isReferencesRendered;

    private static final String PATH = "C:/TO_PARSE/";
    private String fileName;
	
	private void fillDoc(Doc document){
		List<Type> docTypes = new LinkedList<>();
        List<Attribute> docAttributes = new LinkedList<>();
		for (ProxyGenericEntity<Type> type: types){
			if (type.style <3 ){
				docTypes.add(type.entity);
                docAttributes.addAll(type.entity.getAttributes());
			}
		}

		List<ListValueType> docListTypes = new LinkedList<>();
		for (ProxyGenericEntity<ListValueType> listType: listTypes){
			if (listType.style == 1){
				docListTypes.add(listType.entity);
			}
		}
		
		List<ListValue> docListValues = new LinkedList<>();
		for (ProxyGenericEntity<ListValue> listValue: listValues){
			if (listValue.style == 1){
				docListValues.add(listValue.entity);
			}
		}
        document.setTypes(docTypes);
        document.setAttributes(docAttributes);
        document.setListValueTypes(docListTypes);
        document.setListValues(docListValues);
    }
	
	private String getHash(){

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(Files.readAllBytes(Paths.get(PATH + fileName)));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] digest = md.digest();
		
		return DatatypeConverter.printHexBinary(digest).toUpperCase();
	}
	
	private void createDoc(){
		Doc document = new Doc();
		document.setName(fileName);
		
		fillDoc(document);
		document.setHash(getHash());
		Log.info("trying to persist document with name "+document.getName()+ " and hash "+document.getHash());

        DAO.add(document);
        Log.info("id after persist "+document.getId());
	}

    public String saveChanges(){

        for (ProxyGenericEntity entity: entities){
            if(entity.entity instanceof Attribute){
                continue;
            }
            switch (entity.style){
                case 1:
                    Log.info("Persisting: " + entity.entity);
                    DAO.add(entity.entity);
                    Log.info("Persisted: " + entity.entity);
                case 2:
                    Log.info("Updating: " + entity.entity);
                    entity.entity=DAO.update(entity.entity);
                    Log.info("Updated: " + entity.entity);
                    break;
                case 3:
                    Log.info("Deleting: " + entity.entity);
                    DAO.delete(entity.entity);
                    Log.info("Deleted: " + entity.entity);
                    break;
            }
            //Log.info(entity.entity.toString());
        }

        createDoc();
        return "successful_upload?faces-redirect=true";
    }

    private void getEntities(){
      //  Parser parser = new Parser();
//        TypesParser typesParser = new TypesParser();
//        typesParser.parseObjectTypes(PATH_TO_FILE);

        MultiParser MParser = new MultiParser(PATH + fileName);
        entities = new LinkedList<>();
		MParser.parse();
        List<GenericEntity> tmpEntities = MParser.getNewEntities();
        for (GenericEntity entity: tmpEntities){
            entities.add(new ProxyGenericEntity(1, entity));
        }

        tmpEntities = MParser.getUpdateEntities();
        for (GenericEntity entity: tmpEntities){
            entities.add(new ProxyGenericEntity(2, entity));
        }

        tmpEntities = MParser.getDeleteEntities();
        for (GenericEntity entity: tmpEntities){
            entities.add(new ProxyGenericEntity(3, entity));
        }
    }
    private void fillCollections(){
        types = new LinkedList<>();
        attributes = new LinkedList<>();
        listTypes = new LinkedList<>();
        listValues = new LinkedList<>();
        objects = new LinkedList<>();
        values = new LinkedList<>();
        references = new LinkedList<>();

        for (ProxyGenericEntity entity: entities){
            if (entity.entity instanceof Type){
                types.add(entity);
            } else if (entity.entity instanceof Attribute){
                attributes.add(entity);
            } else if(entity.entity instanceof  ListValueType){
                listTypes.add(entity);
            } else if (entity.entity instanceof ListValue){
                listValues.add(entity);
            } else if (entity.entity instanceof ObjectModel){
                objects.add(entity);
            } else if (entity.entity instanceof Value){
                values.add(entity);
            } else if(entity.entity instanceof Reference){
                references.add(entity);
            } else {
                Log.warn("Unknown entity");
            }
        }
    }

    private void showNonEmptyTables(){
        if (!types.isEmpty()){
            isTypeRendered = true;
        }
        if (!attributes.isEmpty()){
            isAttributeRendered = true;
        }
        if (!listTypes.isEmpty()){
            isListsRendered = true;
        }
        if (!listValues.isEmpty()){
            isListValuesRendered = true;
        }
        if (!objects.isEmpty()){
            isObjectsRendered = true;
        }
        if(!values.isEmpty()){
            isValuesRendered = true;
        }
        if(!references.isEmpty()){
            isReferencesRendered = true;
        }
    }


    public void init(){
        Log.info("filename = "+fileName);
        getEntities();

        fillCollections();
        showNonEmptyTables();
    }

    public List<ProxyGenericEntity<Type>> getTypes(){
        return types;
    }

    public List<ProxyGenericEntity<Attribute>> getAttributes(){
        return attributes;
    }

    public void setTypes(List<ProxyGenericEntity<Type>> types) {
        this.types = types;
    }

    public void setAttributes(List<ProxyGenericEntity<Attribute>> attributes) {
        this.attributes = attributes;
    }

    public List<ProxyGenericEntity<ListValueType>> getListTypes() {
        return listTypes;
    }

    public void setListTypes(List<ProxyGenericEntity<ListValueType>> listTypes) {
        this.listTypes = listTypes;
    }

    public List<ProxyGenericEntity<ListValue>> getListValues() {
        return listValues;
    }

    public void setListValues(List<ProxyGenericEntity<ListValue>> listValues) {
        this.listValues = listValues;
    }

    public List<ProxyGenericEntity<ObjectModel>> getObjects() {
        return objects;
    }

    public void setObjects(List<ProxyGenericEntity<ObjectModel>> objects) {
        this.objects = objects;
    }

    public List<ProxyGenericEntity<Value>> getValues() {
        return values;
    }

    public void setValues(List<ProxyGenericEntity<Value>> values) {
        this.values = values;
    }

    public List<ProxyGenericEntity<Reference>> getReferences() {
        return references;
    }

    public void setReferences(List<ProxyGenericEntity<Reference>> references) {
        this.references = references;
    }

    public boolean isAttributeRendered() {
        return isAttributeRendered;
    }

    public void setAttributeRendered(boolean attributeRendered) {
        isAttributeRendered = attributeRendered;
    }

    public boolean isTypeRendered() {
        return isTypeRendered;
    }

    public void setTypeRendered(boolean typeRendered) {
        isTypeRendered = typeRendered;
    }

    public boolean isListsRendered() {
        return isListsRendered;
    }

    public void setListsRendered(boolean listsRendered) {
        isListsRendered = listsRendered;
    }

    public boolean isListValuesRendered() {
        return isListValuesRendered;
    }

    public void setListValuesRendered(boolean listValuesRendered) {
        isListValuesRendered = listValuesRendered;
    }

    public boolean isObjectsRendered() {
        return isObjectsRendered;
    }

    public void setObjectsRendered(boolean objectsRendered) {
        isObjectsRendered = objectsRendered;
    }

    public boolean isValuesRendered() {
        return isValuesRendered;
    }

    public void setValuesRendered(boolean valuesRendered) {
        isValuesRendered = valuesRendered;
    }

    public boolean isReferencesRendered() {
        return isReferencesRendered;
    }

    public void setReferencesRendered(boolean referencesRendered) {
        isReferencesRendered = referencesRendered;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
