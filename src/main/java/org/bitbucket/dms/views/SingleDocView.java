package org.bitbucket.dms.views;

import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.dao.ReferenceDAO;
import org.bitbucket.dms.dao.ValueDAO;
import org.bitbucket.dms.docxParser.DocCreator;
import org.bitbucket.dms.model.Doc;
import org.bitbucket.dms.model.ObjectModel;
import org.bitbucket.dms.model.Reference;
import org.bitbucket.dms.model.Value;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.InputStream;
import java.util.List;

@ViewScoped
@ManagedBean
public class SingleDocView {

    private Doc document;
    private StreamedContent file;


    private Long id;

    @EJB
    private GenericDAO dao;

    Logger log = LoggerFactory.getLogger(SingleDocView.class);

    private void initFields(){
//        name = object.getName();
//        description = object.getDescription();
//        type = object.getType();
//        parent = object.getParent();
//        project = object.getProjectID();
//        order = object.getOrderID();
    }


//    @PostConstruct
    public void init() {
        log.info("SingleObject_view initialization");
        log.info("Id = " + id);
        document=(Doc)dao.getById(Doc.class,id);
        if(document == null) {
            log.warn("ID not found!!!");
            return;
        }
        initFields();
    }

    public Doc getDocument() {
        return document;
    }
    public void createDocument() {

        DocCreator creator=new DocCreator();
        creator.createDoc(id);

    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }



    public StreamedContent getFile() {
        return file;
    }

    //
//    public String getName() { return name; }
//    public void setName(String name) { this.name = name; }
//
//    public Type getType() { return type; }
//    public void setType(Type type) { this.type = type; }
//
//    public String getDescription() { return description; }
//    public void setDescription(String description) { this.description = description; }
//
//    public ObjectModel getParent() { return parent; }
//    public void setParent(ObjectModel parent) { this.parent = parent; }
//
//    public ObjectModel getProject() { return  project; }
//    public void setProject(ObjectModel project) { this.project = project; }
//
//    public Long getOrder() { return order; }
//    public void setOrder(Long order) { this.order = order; }

}
