package org.bitbucket.dms.docxParser;

import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.bitbucket.dms.model.GenericEntity;
import org.bitbucket.dms.model.ListValue;
import org.bitbucket.dms.model.ListValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ListsParser extends BaseParser{
	
	private static Logger Log = LoggerFactory.getLogger(ListsParser.class.getName());
	
	private final static String META_LISTS = "{META: Lists}";

	ListValueType LVType;

	private String getName(){
		String tmp = null;
		do {
			if (!iterator.hasNext()){
				Log.info("End of Lists parser");
				end = true;
				break;
			}
			tmp = getTextFromParagraph(iterator.next());
		} while ((tmp == null || tmp.equals("")) && !end );
		if (tmp == null || tmp.equals("")){
			end = true;
			return null;
		} else {
			if (isEndTag(tmp)) {
				end = true;
				return null;
			}
			return tmp;
		}
	}

	private List<ListValue> getValues(){
		List<ListValue> listValues = new LinkedList<>();
		while(iterator.hasNext()){
			IBodyElement tempElem;
			tempElem = iterator.next();
			if (isTable(tempElem)){
				List<ListValue> values = new LinkedList<>();
				ListValue tmp;
				List<List<String>> rows = readRowsFromTable((XWPFTable)tempElem);
				boolean isFirst = true;
				for(List<String> row: rows){
					if (isFirst){
						isFirst = false;
						continue;
					}
					tmp = new ListValue();
					Log.info(tmp.toString());
					int tempIdType = checkTableID(row.get(0));
					if (tempIdType == 2){
						tmp.setListValueId(getTableID(row.get(0)));
						deleteEntities.add(tmp);
					} else if (tempIdType == 1){
						tmp.setListValueId(getTableID(row.get(0)));
						updateEntities.add(tmp);
					} else {
						tmp.setListValueId(null);
						newEntities.add(tmp);
					}
					tmp.setValue(row.get(1));
					values.add(tmp);
					tmp.setListValueType(LVType);
				}
				return values;
			}
		}
		return null;
	}

	private void setIdOrValues(ListValueType type){
		String tmp;
		do {
			tmp = getTextFromParagraph(iterator.next());
		} while (!tmp.equalsIgnoreCase("Values") && checkID(tmp) == 0 && iterator.hasNext());
		if (!iterator.hasNext()){
			end = true;
		} else if (tmp.equalsIgnoreCase("Values")) {
			LVType = type;
			if (type.getId() == null){
				newEntities.add(type);
			}
			type.setListValues(getValues());
		} else if(checkID(tmp) > 0){
			type.setId(getID(tmp));
			if (checkID(tmp) == 2){
				deleteEntities.add(type);
			} else if (checkID(tmp) == 1){
				updateEntities.add(type);
			}
			setIdOrValues(type);
		}
	}

	private ListValueType getList(){
		ListValueType tmp = new ListValueType();
		tmp.setName(getName());
		if (end){
			return null;
		}
		if (tmp.getName() == null) {
			Log.warn("Can't find name");
			return null;
		}
		setIdOrValues(tmp);
		return tmp;
	}
	
	@Override
	public void parse(Iterator<IBodyElement> iterator){
		super.parse(iterator);
		while(iterator.hasNext() && !end) {
			getList();
		}
	}
	
	public ListsParser() {
		super();
    }

	@Deprecated
    public ListsParser(String path) {
		super(path);
    }
	
	public ListsParser(Iterator<IBodyElement> iterator){
		super(iterator);
	}
}