package org.bitbucket.dms.docxParser;

import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.bitbucket.dms.dao.DataTypeDAO;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.DataType;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class TypesParser extends BaseParser {

	private static Logger Log = LoggerFactory.getLogger(TypesParser.class.getName());

	private String getObjectTypeDescription(Type type) {

        while (iterator.hasNext()) {
            IBodyElement tmp = iterator.next();
            String str = getTextFromParagraph(tmp);

            int id_type = checkID(str);
            Log.info("ID Type : " + id_type);
            if (id_type > 0) {
                type.setId(getID(str));
                if (id_type == 2) {
                    deleteEntities.add(type);
                } else if (id_type == 1) {
                    updateEntities.add(type);
                }
            } else if (getTextFromParagraph(tmp).equalsIgnoreCase("Description")) {
                if (type.getId() == null) {
                    newEntities.add(type);
                }
                return getTextFromParagraph(iterator.next());
            }
        }
        Log.warn("Description not found!");
        return null;
    }

    private String getObjectTypeProperties() {
        while (iterator.hasNext() &&
                !getTextFromParagraph(iterator.next()).equalsIgnoreCase("Properties")) {
        }
        if (!iterator.hasNext())
            return null;
        IBodyElement element = iterator.next();
        while (!isTable(element)) {
            element = iterator.next();
        }
        List<List<String>> properties = readRowsFromTable((XWPFTable) element);

        StringBuilder tmp = new StringBuilder();
        int k = 0;
        for (List<String> row : properties) {
            if (k == 0) {
                k++;
                continue;
            }
            tmp.append(row.get(0)).append(": ").append(row.get(1)).append("; ");
        }
        return tmp.toString();
    }

    private List<Attribute> getObjectTypeAttributes() {
        while (iterator.hasNext() &&
                !getTextFromParagraph(iterator.next()).equalsIgnoreCase("Attributes")) {
        }
        if (!iterator.hasNext())
            return null;

        IBodyElement element = iterator.next();
        while (!isTable(element)) {
            element = iterator.next();
        }
        List<List<String>> table = readRowsFromTable((XWPFTable) element);

        List<Attribute> attributes = new LinkedList<>();
        Attribute tmp;
        Long tmpId;

        boolean isFirst = true;
        for (List<String> row : table) {
            if (isFirst) {
                isFirst = false;
                continue;
            }
            tmp = new Attribute();

            String tmpString = row.get(0);
            switch (checkTableID(tmpString)) {
                case 2:
                    deleteEntities.add(tmp);
                    break;
                case 1:
                    updateEntities.add(tmp);
                    break;
                default:
                    attributes.add(tmp);
                    newEntities.add(tmp);
                    break;
            }
            tmp.setId(getTableID(tmpString));
            getAttribute(row, tmp);

//            Log.info(tmp.getName() + " : " + tmp.getProperties() + " : " + tmp.getDescription())
        }
        return attributes;
    }

	private void getAttribute(List<String> row, Attribute tmp) {
        tmp.setName(row.get(1));
        String type = row.get(2);
        DataTypeDAO dao;

        try {
            dao = (DataTypeDAO) new InitialContext().lookup("java:module/DataTypeDAO");
            if (dao==null) Log.error("******************** DAO IS NULL!!!!! ****************");

        } catch (NamingException e) {
            throw new EJBException(e);
        }


            List<DataType> dataTypeList = dao.getByName(type);
            if(!dataTypeList.isEmpty()) tmp.setDataType(dataTypeList.get(0));
            else {
                Log.error("This type not found");
            }

        tmp.setProperties(row.get(3));
        tmp.setDescription(row.get(4));
    }

    private Type getObjectType() {
        Type type = new Type();

        type.setName(getObjectTypeName());
        if (type.getName() == null) {
            return null;
        }
        type.setDescription(getObjectTypeDescription(type));
        type.setProperties(getObjectTypeProperties());
        Log.info(type.toString());
        type.setAttributes(getObjectTypeAttributes());

        return type;
    }

    private String getObjectTypeName() {
        String tmp;
        do {
            if (!iterator.hasNext()) {
                end = true;
                return null;
            }
            tmp = getTextFromParagraph(iterator.next());
            if (isEndTag(tmp)) {
                end = true;
                return null;
            }
        } while (tmp == null || tmp.trim().equals(""));
        return tmp;
    }


    @Deprecated
    public void parseObjectTypes(String path) {
        openDoc(path);

//        goToStart(META_OBJECT_TYPE);
        Type type = null;
        while (iterator.hasNext()) {
            type = getObjectType();
            if (type != null) {
//                Log.info(type.toString());
                newEntities.add(type);
            }
        }
    }
	
	/*
	private void getObjectTypes(Iterator<IBodyElement> iterator){
        end = false;
		this.iterator = iterator;
		
		Type type = null;
		while (!end && iterator.hasNext()) {
				type = getObjectType();

		}

	}*/

    @Override
    public void parse(Iterator<IBodyElement> iterator) {
        end = false;
        this.iterator = iterator;

        Type type = null;
        while (!end && iterator.hasNext()) {
            type = getObjectType();
        }
    }

    public TypesParser() {
        super();
    }

    @Deprecated
    public TypesParser(String path) {
        super(path);
    }

    public TypesParser(Iterator<IBodyElement> iterator) {
        super(iterator);
    }

}