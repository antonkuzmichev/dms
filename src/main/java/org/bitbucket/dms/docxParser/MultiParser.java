package org.bitbucket.dms.docxParser;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.bitbucket.dms.model.GenericEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MultiParser {
	
	private static Logger Log = LoggerFactory.getLogger(MultiParser.class.getName());

	protected final static String PARAGRAPH = "PARAGRAPH";
	
	private final static String META_TYPES = "{META: Object Type}";
	private final static String TYPES_SIGNATURE = "Object Type";
	private final static String META_LISTS = "{META: Lists}";
	private final static String LISTS_SIGNATURE = "Lists";
	private final static String META_OBJECTS = "{META: Objects}";
	private final static String OBJECTS_SIGNATURE = "Objects";
	
	private final static String META_REGEXP = "\\{\\s*META\\s*:\\s*[\\w\\s]+\\}";
	
	
    private FileInputStream file;
	private Iterator<IBodyElement> iterator;
	
	TypesParser typesParser;
	ListsParser listsParser;
	ObjectsParser objectsParser;
	
	private List<GenericEntity> newEntities;
    private List<GenericEntity> updateEntities;
    private List<GenericEntity> deleteEntities;
	
	private Iterator<IBodyElement> openDoc(String path) {
        try {
            file = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            Log.warn("File not found!", e);
            e.printStackTrace();
        }
        XWPFDocument xdoc = null;
        try {
            xdoc = new XWPFDocument(OPCPackage.open(file));
        } catch (IOException e) {
            Log.warn("IOException: ", e);
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            Log.warn("InvalidFormat: ", e);
            e.printStackTrace();
        }
        iterator = xdoc.getBodyElementsIterator();
        return iterator;
    }
	
	private String getTextFromParagraph(IBodyElement element) {
        StringBuilder tmp = new StringBuilder();
        if (PARAGRAPH.equalsIgnoreCase(element.getElementType().name())) {
            XWPFParagraph paragraph = (XWPFParagraph) element;
            List<XWPFRun> lines = paragraph.getRuns();
            for (XWPFRun line : lines) {
                tmp.append(line.text());
            }
            return tmp.toString();
        }
        return null;
    }
	
	private void sendToParser(String meta){
		if (meta.contains(TYPES_SIGNATURE)){
			typesParser.parse(iterator);
		} else if (meta.contains(LISTS_SIGNATURE)){
			listsParser.parse(iterator);
		} else if (meta.contains(OBJECTS_SIGNATURE)){
			objectsParser.parse(iterator);
		}
	}
	
	public void parse(){
		if (iterator == null){
			Log.warn("You should open file first!");
			return;
		}
	
		Pattern pattern = Pattern.compile(META_REGEXP, Pattern.CASE_INSENSITIVE);
		Matcher matcher;
		
		while (iterator.hasNext()) {
			String tmp = getTextFromParagraph(iterator.next());
			if (tmp == null){
				continue;
			}
			matcher =  pattern.matcher(tmp);
			if (matcher.find()) {
				Log.info("Meta is found! " + matcher.group());



				sendToParser(matcher.group());
			}
		}
		
		newEntities.addAll(typesParser.getNewEntities());
		newEntities.addAll(listsParser.getNewEntities());
		newEntities.addAll(objectsParser.getNewEntities());
		
		updateEntities.addAll(typesParser.getUpdateEntities());
		updateEntities.addAll(listsParser.getUpdateEntities());
		updateEntities.addAll(objectsParser.getUpdateEntities());
		
		deleteEntities.addAll(typesParser.getDeleteEntities());
		deleteEntities.addAll(listsParser.getDeleteEntities());
		deleteEntities.addAll(objectsParser.getDeleteEntities());

		if (file != null){
			try {
				file.close();
			} catch(IOException e){
				e.printStackTrace();
			}
		}
		//Log.info(newEntities.toString());
	}
	
	public void parse(String path){
		if (file != null){
			try {
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		openDoc(path);
		
		parse();
	}
	
	public List<GenericEntity> getNewEntities() {
        return newEntities;
    }

    public List<GenericEntity> getUpdateEntities() {
        return updateEntities;
    }

    public List<GenericEntity> getDeleteEntities() {
        return deleteEntities;
    }
	
	public MultiParser(){
		typesParser = new TypesParser();
		listsParser = new ListsParser();
		objectsParser = new ObjectsParser();

		newEntities = new LinkedList<>();
		updateEntities = new LinkedList<>();
		deleteEntities = new LinkedList<>();
	}
	
	public MultiParser(String path){
		this();
		openDoc(path);
	}
}