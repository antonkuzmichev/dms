package org.bitbucket.dms.docxParser;


import org.apache.poi.xwpf.usermodel.*;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.dao.TypeDAO;
import org.bitbucket.dms.model.*;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DocCreator {
	private final static String META_TYPES = "{META: Object Type}";
	private final static String META_LISTS = "{META: Lists}";
	private final static String META_OBJECTS = "{META: Objects}";
	
	private static final String PROPERTIES_REGEX = "[.[^;]]+;";
	private static final String DIVIDER_REGEX = "[.[^:]]+:";
	
	List<Type> types;
	List<Attribute> attributes;
	
	List<ListValueType> listTypes;
	List<ListValue> listValue;
	
	List<ObjectModel> objects;
	List<Value> values;
	List<Reference> references;
	
	XWPFDocument document=new XWPFDocument();

    GenericDAO dao;


    private void placeTypes(){
		XWPFParagraph p1 = document.createParagraph();
		XWPFRun r1 = p1.createRun();
		r1.setText(META_TYPES);
		
		for (Type type: types){
			
			XWPFParagraph p2 = document.createParagraph();
			p2.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun r2 = p2.createRun();
			r2.setBold(true);
			r2.setText(type.getName());
			
			XWPFParagraph p3 = document.createParagraph();
			XWPFRun r3 = p3.createRun();
			r3.setText("ID=" + type.getId());
			
			XWPFParagraph p4 = document.createParagraph();
			XWPFRun r4 = p4.createRun();
			r4.setBold(true);
			r4.setText("Description");
			
			XWPFParagraph p5 = document.createParagraph();
			XWPFRun r5 = p5.createRun();
			r5.setText(type.getDescription());
			
			XWPFParagraph p6 = document.createParagraph();
			XWPFRun r6 = p6.createRun();
			r6.setBold(true);
			r6.setText("Properties");
			
			
			
			Pattern propPattern = Pattern.compile(PROPERTIES_REGEX);
			Matcher mtchr = propPattern.matcher(type.getProperties());
			
			List<String> properties = new LinkedList<>();
			while (mtchr.find()){
				properties.add(mtchr.group());
			}

			Pattern dividerPattern = Pattern.compile(DIVIDER_REGEX);
			List<String> tmpListString;
			List<List<String>> propertiesTable = new LinkedList<>();
			for (String str: properties){
				mtchr = dividerPattern.matcher(str);
				if (mtchr.find()){
					tmpListString = new LinkedList<>();

					tmpListString.add(str.substring(0, mtchr.end()));
					tmpListString.add(str.substring(mtchr.end() + 1));
					propertiesTable.add(tmpListString);
				}
			}
			
			XWPFTable propTable = document.createTable(propertiesTable.size() + 1, 2);
			XWPFParagraph tablePropParagraph = propTable.getRow(0).getCell(0).getParagraphs().get(0);
			tablePropParagraph.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun tablePropRun = tablePropParagraph.createRun();
			tablePropRun.setBold(true);
			tablePropRun.setText("Property");
			
			XWPFParagraph tablePropValue = propTable.getRow(0).getCell(1).getParagraphs().get(0);
			tablePropValue.setAlignment(ParagraphAlignment.CENTER);
			//XWPFRun
			tablePropRun = tablePropValue.createRun();
			tablePropRun.setBold(true);
			tablePropRun.setText("Value");
			
			for(int i = 0; i < propertiesTable.size(); i++){
				propTable.getRow(i + 1).getCell(0).setText(propertiesTable.get(i).get(0));
				propTable.getRow(i + 1).getCell(1).setText(propertiesTable.get(i).get(1));
			}
			
			
			XWPFParagraph p7 = document.createParagraph();
			XWPFRun r7 = p7.createRun();
			r7.setBold(true);
			r7.setText("Attributes");
			
			List<Attribute> attributes = type.getAttributes();
			
			
			XWPFTable attributesTable = document.createTable(attributes.size() + 1, 5);
			
			XWPFParagraph attributeId = attributesTable.getRow(0).getCell(0).getParagraphs().get(0);
			attributeId.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun atrIdRun = attributeId.createRun();
			atrIdRun.setBold(true);
			atrIdRun.setText("ID");
			
			XWPFParagraph attributeName = attributesTable.getRow(0).getCell(1).getParagraphs().get(0);
			attributeName.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun atrNameRun = attributeName.createRun();
			atrNameRun.setBold(true);
			atrNameRun.setText("Name");
			
			XWPFParagraph attributeType = attributesTable.getRow(0).getCell(2).getParagraphs().get(0);
			attributeType.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun atrTypeRun = attributeType.createRun();
			atrTypeRun.setBold(true);
			atrTypeRun.setText("Type");
			
			XWPFParagraph attributeProperies = attributesTable.getRow(0).getCell(3).getParagraphs().get(0);
			attributeProperies.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun atrPropRun = attributeProperies.createRun();
			atrPropRun.setBold(true);
			atrPropRun.setText("Properties");
			
			XWPFParagraph attributeDescription = attributesTable.getRow(0).getCell(4).getParagraphs().get(0);
			attributeDescription.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun atrDescrRun = attributeDescription.createRun();
			atrDescrRun.setBold(true);
			atrDescrRun.setText("Description");
			
			for(int i = 0; i < attributes.size(); i++){
				attributesTable.getRow(i + 1).getCell(0).setText(attributes.get(i).getId().toString());
				attributesTable.getRow(i + 1).getCell(1).setText(attributes.get(i).getName());
				attributesTable.getRow(i + 1).getCell(2).setText(attributes.get(i).getTypeName());
				attributesTable.getRow(i + 1).getCell(3).setText(attributes.get(i).getProperties());
				attributesTable.getRow(i + 1).getCell(4).setText(attributes.get(i).getDescription());
			}
		}
	}
	
	private void placeLists(){
		
	}
	
	private void placeObjects(){
		
	}
	
	public File createDoc(Long documentId){
        try {
            dao = (GenericDAO) new InitialContext().lookup("java:module/GenericDAO");
        } catch (NamingException e) {
            e.printStackTrace();
        }
        Doc doc= (Doc) dao.getById(Doc.class, documentId);
        types=doc.getTypes();
        if (!types.isEmpty()){
			placeTypes();
		}
//		if (!listTypes.isEmpty()){
//			placeLists();
//		}
//		if (!objects.isEmpty()){
//			placeObjects();
//		}

        FileOutputStream out;
        try {
            out = new FileOutputStream("c:/to_parse/"+doc.getName()+"-"+doc.getHash()+".docx");
            document.write(out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

	}
}
