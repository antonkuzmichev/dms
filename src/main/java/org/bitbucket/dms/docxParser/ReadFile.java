package org.bitbucket.dms.docxParser;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.IBody;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;
@Deprecated
public class ReadFile {
    public static void main(String[] args) {
        try {
            FileInputStream fis = new FileInputStream("dms\\docs\\Task2_Objects_and_Views.docx");
            XWPFDocument xdoc=new XWPFDocument(OPCPackage.open(fis));
            Iterator<IBodyElement> bodyElementIterator = xdoc.getBodyElementsIterator();
            while(bodyElementIterator.hasNext()) {
                IBodyElement element = bodyElementIterator.next();
                if("TABLE".equalsIgnoreCase(element.getElementType().name())) {
                    List<XWPFTable> tableList =  element.getBody().getTables();
                    for (XWPFTable table: tableList){
                        System.out.println(table.getText());
                    }
                }
            }
 		  /*FileOutputStream out = new FileOutputStream("Data.xml");
 		    xdoc.write(out);
 		    out.close();*/

        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
} 