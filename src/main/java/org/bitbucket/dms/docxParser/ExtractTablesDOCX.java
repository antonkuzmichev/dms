package org.bitbucket.dms.docxParser;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.*;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
@Deprecated
public class ExtractTablesDOCX {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 try {
				FileInputStream fis = new FileInputStream("dms\\docs\\Task1_Object_type_PLAN_with_attributes_binded.docx");
				XWPFDocument xdoc=new XWPFDocument(OPCPackage.open(fis));
				List<IBodyElement> docIter = xdoc.getBodyElements();
			    Iterator<IBodyElement>  bodyElementIterator = docIter.iterator();
				
				while(bodyElementIterator.hasNext()) {
				  IBodyElement element = bodyElementIterator.next();
			          if("TABLE".equalsIgnoreCase(element.getElementType().name())) {
				    
			        	  List<XWPFTable> tableList = xdoc.getTables(); 
			        	  for (XWPFTable table: tableList){
				        System.out.println("Total Number of Rows of Table:"+table.getNumberOfRows());
					System.out.println(table.getText());
				     }
				  }
			        }
			    } catch(Exception ex) {
				ex.printStackTrace();
			    } 

	}

}
