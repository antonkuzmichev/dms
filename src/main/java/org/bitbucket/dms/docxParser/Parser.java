package org.bitbucket.dms.docxParser;


import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.GenericEntity;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.xml.crypto.Data;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

@Deprecated
public class Parser {
    /*
    private enum DataType{
        Integer(1L),
        String(2L),
        Date(3L),
        List(4L),
        Reference(5L);

        private final long value;
        DataType(long value){ this.value = value; }

        public long getValue(){
            return value;
        }
    }

    private static Logger Log = LoggerFactory.getLogger(Parser.class.getName());

    @EJB
    private GenericDAO<Type, Long> typeDAO;

    private final static String PARAGRAPH = "PARAGRAPH";
    private final static String TABLE = "TABLE";

    private final static String REMOVE_ANNOTATION = "@Remove";

    private final static String META_OBJECT_TYPE = "{META: Object Type}";

    private FileInputStream file;
    private Iterator<IBodyElement> iterator;

    private List<GenericEntity> newEntities;
    private List<GenericEntity> updateEntities;
    private List<GenericEntity> deleteEntities;


    private Iterator<IBodyElement> openDoc(String path) {
        try {
            file = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            Log.warn("File not found!", e);
            e.printStackTrace();
        }
        XWPFDocument xdoc = null;
        try {
            xdoc = new XWPFDocument(OPCPackage.open(file));
        } catch (IOException e) {
            Log.warn("IOException: ", e);
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            Log.warn("InvalidFormat: ", e);
            e.printStackTrace();
        }
        iterator = xdoc.getBodyElementsIterator();
        return iterator;

    }

    private String getTextFromParagraph(IBodyElement element) {
        StringBuilder tmp = new StringBuilder();
        if (PARAGRAPH.equalsIgnoreCase(element.getElementType().name())) {
            XWPFParagraph paragraph = (XWPFParagraph) element;
            List<XWPFRun> lines = paragraph.getRuns();
            for (XWPFRun line : lines) {
                tmp.append(line.text());
            }
            return tmp.toString();
        }
        return null;
    }


    private void goToStart(String meta) {
        while (iterator.hasNext()) {
            IBodyElement element = iterator.next();
            if (getTextFromParagraph(element).equalsIgnoreCase(meta)) {
                Log.info("Meta is found! " + meta);
                return;
            }
        }
        Log.warn("Meta not found!");
    }

    private String getObjectTypeDescription() {
        while (iterator.hasNext()) {
            if (getTextFromParagraph(iterator.next()).equalsIgnoreCase("Description")) {
                return getTextFromParagraph(iterator.next());
            }
        }
        Log.warn("Description not found!");
        return null;
    }

    private boolean isTable(IBodyElement element) {
        if (element.getElementType().name().equalsIgnoreCase(TABLE))
            return true;
        return false;
    }

    private List<List<String>> readRowsFromTable(XWPFTable table) {
        StringBuilder tmp;
        List<List<String>> tableText = new LinkedList<>();
        List<XWPFTableRow> rows = table.getRows();
        for (XWPFTableRow row : rows) {
            List<String> rowText = new LinkedList<>();
            List<XWPFTableCell> cells = row.getTableCells();
            for (XWPFTableCell cell : cells) {
                tmp = new StringBuilder();
                List<XWPFParagraph> paragraphs = cell.getParagraphs();
                for (XWPFParagraph paragraph : paragraphs) {
                    tmp.append(getTextFromParagraph(paragraph));
                }
                rowText.add(tmp.toString());
            }
            tableText.add(rowText);
        }
        return tableText;
    }

    private String getObjectTypeProperties() {
        while (iterator.hasNext() &&
                !getTextFromParagraph(iterator.next()).equalsIgnoreCase("Properties")) {
        }
        if (!iterator.hasNext())
            return null;
        IBodyElement element = iterator.next();
        while (!isTable(element)) {
            element = iterator.next();
        }
        List<List<String>> properties = readRowsFromTable((XWPFTable) element);

        StringBuilder tmp = new StringBuilder();
        int k = 0;
        for (List<String> row : properties) {
            if (k == 0) {
                k++;
                continue;
            }
            tmp.append(row.get(0)).append(": ").append(row.get(1)).append("; ");
        }
        return tmp.toString();
    }

    private void getAttribute(List<String> row, Attribute tmp) {
        tmp.setName(row.get(1));

        String type = row.get(2);
        if (type.contains("Integer")) {
            tmp.setType(DataType.Integer.getValue());
        } else if (type.contains("String")) {
            tmp.setType(DataType.String.getValue());
        } else if (type.contains("Date")) {
            tmp.setType(DataType.Date.getValue());
        } else if (type.contains("List")) {
            tmp.setType(DataType.List.getValue());
        } else if (type.contains("Reference")){
            tmp.setType(DataType.Reference.getValue());
        } else {
            Log.error("This type not found");
        }

        tmp.setProperties(row.get(3));
        tmp.setDescription(row.get(4));
    }

    private List<Attribute> getObjectTypeAttributes() {
        while (iterator.hasNext() &&
                !getTextFromParagraph(iterator.next()).equalsIgnoreCase("Attributes")) {
        }
        if (!iterator.hasNext())
            return null;

        IBodyElement element = iterator.next();
        while (!isTable(element)) {
            element = iterator.next();
        }
        List<List<String>> table = readRowsFromTable((XWPFTable) element);

        List<Attribute> attributes = new LinkedList<>();
        Attribute tmp;
        Long tmpId;

        boolean isFirst = true;
        for (List<String> row : table) {
            if (isFirst) {
                isFirst = false;
                continue;
            }
            tmp = new Attribute();
            //Log.info(row.get(0));
            tmpId = Long.getLong(row.get(0).replaceAll("[\\D]", ""));
            if (row.get(0).matches(".*\\d+.*")) {
                Scanner scanner = new Scanner(row.get(0));
                tmpId = scanner.nextLong();
            } else {
                tmpId = null;
            }
            Log.info("The id is " + tmpId);
            if (tmpId == null) {
                tmpId = 0L;
            }

            if (row.get(0).contains(REMOVE_ANNOTATION)) {
                //delete
                tmp.setId(tmpId);
                getAttribute(row, tmp);
                deleteEntities.add(tmp);
            } else if (tmpId != 0) {
                //update
                tmp.setId(tmpId);
                getAttribute(row, tmp);
                updateEntities.add(tmp);
            } else {
                //create
                getAttribute(row, tmp);
                newEntities.add(tmp);
            }
            Log.info(tmp.getName() + " : " + tmp.getProperties() + " : " + tmp.getDescription());
            attributes.add(tmp);
        }
        return attributes;
    }

    private Type getObjectType() {
        Type type = new Type();

        type.setName(getObjectTypeName());
        type.setDescription(getObjectTypeDescription());
        type.setProperties(getObjectTypeProperties());
        Log.info(type.toString());
        type.setAttributes(getObjectTypeAttributes());
        if (type.getName() != null) {
            return type;
        } else {
            return null;
        }
    }

    private String getObjectTypeName(){
        String tmp;
        do {
            if (!iterator.hasNext()){
                return null;
            }
            tmp = getTextFromParagraph(iterator.next());
        } while (tmp == null || tmp.trim().equals(""));
            return tmp;
    }

    public List<GenericEntity> getNewEntities() {
        return newEntities;
    }

    public List<GenericEntity> getUpdateEntities() {
        return updateEntities;
    }

    public List<GenericEntity> getDeleteEntities() {
        return deleteEntities;
    }

    public void parseObjectTypes(String path) {
        openDoc(path);

        goToStart(META_OBJECT_TYPE);
        Type type = null;
        while (iterator.hasNext()) {
            type = getObjectType();
            if (type != null) {
//                Log.info(type.toString());
                newEntities.add(type);
            }
        }
    }

    public Parser() {
        newEntities = new LinkedList<>();
        updateEntities = new LinkedList<>();
        deleteEntities = new LinkedList<>();
    }

    public Parser(String path) {
        newEntities = new LinkedList<>();
        updateEntities = new LinkedList<>();
        deleteEntities = new LinkedList<>();
        openDoc(path);
    }
    */
}
