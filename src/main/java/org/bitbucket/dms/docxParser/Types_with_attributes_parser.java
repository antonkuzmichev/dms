package org.bitbucket.dms.docxParser;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.List;

@Deprecated
public class Types_with_attributes_parser {
    private static Logger Log = LoggerFactory.getLogger(Types_with_attributes_parser.class.getName());

    private static boolean isMeta(IBodyElement element) {
        if ("PARAGRAPH".equalsIgnoreCase(element.getElementType().name())) {
            XWPFParagraph paragraph = (XWPFParagraph) element;
            List<XWPFRun> lines = paragraph.getRuns();
            for (XWPFRun line : lines) {
                if (line.text().equalsIgnoreCase("{META: Object Type}"))
                    return true;
            }
        }
        return false;
    }

    public static void parse() {
        Type type = new Type();
//            +1) найти {META: Object Type}
//            2) следующая строка - объектный тип Plan
//            3) Description
//            4) text
//            5) Properties
//            6) Table with properties (2 collumns)
//            7) Attributes
//            8) Table with Attributes (4 collumns)
//            2a) объектный тип...
        try {
            FileInputStream fis = new FileInputStream("C:\\Types_with_Attributes.docx");
            XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
            List<IBodyElement> bodyDoc = xdoc.getBodyElements();

            String tmp;
            int step = 0;
            for (IBodyElement element : bodyDoc) {
                switch (step) {
                    case 0:
                        if (isMeta(element)) {
                            step++;
                            Log.debug("Meta is found!");
                        }
                        break;
                    case 1:
                        tmp = new String();
                        if ("PARAGRAPH".equalsIgnoreCase(element.getElementType().name())) {
                            XWPFParagraph paragraph = (XWPFParagraph) element;
                            List<XWPFRun> lines = paragraph.getRuns();
                            for (XWPFRun line : lines) {
                                tmp += line.text();
                            }
                            step++;
                            Log.debug("Type name: " + tmp);
                            type.setName(tmp);
                        }
                        break;
                    case 2:
                        if ("PARAGRAPH".equalsIgnoreCase(element.getElementType().name())) {
                            XWPFParagraph paragraph = (XWPFParagraph) element;
                            List<XWPFRun> lines = paragraph.getRuns();
                            tmp = new String();
                            for (XWPFRun line : lines) {
                                tmp += line;
                            }
                            if (tmp.equalsIgnoreCase("Description")){
                                step++;
                                Log.debug("Description header");
                            }
                        }
                        break;
                    case 3:
                        if ("PARAGRAPH".equalsIgnoreCase(element.getElementType().name())) {
                            XWPFParagraph paragraph = (XWPFParagraph) element;
                            List<XWPFRun> lines = paragraph.getRuns();
                            tmp = new String();
                            for (XWPFRun line : lines) {
                                tmp += line;
                            }
                            step++;
                            type.setDescription(tmp);
                            Log.debug("Type description: " + tmp);
                        }
                        break;
                    case 4:
                        if ("PARAGRAPH".equalsIgnoreCase(element.getElementType().name())) {
                            XWPFParagraph paragraph = (XWPFParagraph) element;
                            List<XWPFRun> lines = paragraph.getRuns();
                            tmp = new String();
                            for (XWPFRun line : lines) {
                                tmp += line;
                            }
                            if (tmp.equalsIgnoreCase("Properties")){
                                step++;
                                Log.debug("Properties table header");
                            }
                        }
                        break;
                    case 5:
                        if ("TABLE".equalsIgnoreCase(element.getElementType().name())) {
                            XWPFTable table = (XWPFTable) element;
                            //XWPFTable -> XWPFTableRow -> XWPFTableCell -> XWPFParagraph
                            List<XWPFTableRow> rows = table.getRows();
                            int k = 0;
                            tmp = new String();
                            for (XWPFTableRow row : rows){
                                if(k == 0){
                                    k++;
                                    continue;
                                }
                                List<XWPFTableCell> cells = row.getTableCells();
                                for (XWPFTableCell cell: cells){
                                    List<XWPFParagraph> paragraphs = cell.getParagraphs();
                                    for(XWPFParagraph paragraph: paragraphs){
                                        tmp = tmp + paragraph.getText();
                                    }
                                    if (k % 2 == 1)
                                        tmp = tmp + ": ";
                                    k++;
                                }
                                tmp = tmp + "; ";
                            }
                            Log.debug("Properties: \n" + tmp);
                            type.setProperties(tmp);
                            step++;
                        }
                        break;
                    case 6:
                        if ("PARAGRAPH".equalsIgnoreCase(element.getElementType().name())) {
                            XWPFParagraph paragraph = (XWPFParagraph) element;
                            List<XWPFRun> lines = paragraph.getRuns();
                            tmp = new String();
                            for (XWPFRun line : lines) {
                                tmp += line;
                            }
                            if (tmp.equalsIgnoreCase("Attributes")){
                                step++;
                                Log.debug("Attributes table header");
                            }
                        }
                        break;
                    case 7:
                        if ("TABLE".equalsIgnoreCase(element.getElementType().name())) {
                            List<Attribute> attributes = new LinkedList<>();
                            Attribute attribute;

                            XWPFTable table = (XWPFTable) element;
                            List<XWPFTableRow> rows = table.getRows();
                            int k = 0;
                            int cellNum = 0;
                            tmp = new String();
                            for (XWPFTableRow row : rows){
                                if(k == 0){
                                    k++;
                                    continue;
                                }
                                attribute = new Attribute();
                                List<XWPFTableCell> cells = row.getTableCells();
                                for (XWPFTableCell cell: cells){
                                    List<XWPFParagraph> paragraphs;
                                    switch (cellNum % 4){
                                         case 0:
                                            paragraphs = cell.getParagraphs();
                                            for(XWPFParagraph paragraph: paragraphs){
                                                tmp = tmp + paragraph.getText();
                                            }
                                            attribute.setName(tmp);
                                            cellNum++;
                                            break;
                                        case 1:
                                            paragraphs = cell.getParagraphs();
                                            for(XWPFParagraph paragraph: paragraphs){
                                                tmp = tmp + paragraph.getText();
                                            }
//                                            attribute.setDataType(tmp);
                                            break;
                                        case 2:
                                    }

                                }
                                tmp = tmp + "; ";
                            }
                            Log.debug("Properties: \n" + tmp);
                            type.setProperties(tmp);
                            step++;
                        }
                        break;
                }

            }
            fis.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
