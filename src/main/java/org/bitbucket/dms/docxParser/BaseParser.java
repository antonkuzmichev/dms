package org.bitbucket.dms.docxParser;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;
import org.bitbucket.dms.dao.DataTypeDAO;
import org.bitbucket.dms.model.GenericEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public abstract class BaseParser {
    private static Logger Log = LoggerFactory.getLogger(BaseParser.class.getName());

    protected final static String PARAGRAPH = "PARAGRAPH";
    protected final static String TABLE = "TABLE";

    protected final static String REMOVE_ANNOTATION = "@Remove";

    private final static String META_REGEXP = "\\{\\s*META\\s*:\\s*[\\w\\s]+\\}";
	
	protected final static String ID_REGEXP = "id=\\d+";
	protected final static String REMOVE_ID_REGEXP = ID_REGEXP + " " + REMOVE_ANNOTATION;
	
	protected final static String TABLE_ID_REGEXP = "\\d+";
	protected final static String TABLE_REMOVE_ID_REGEXP = TABLE_ID_REGEXP + " " + REMOVE_ANNOTATION;

	@Deprecated
	protected FileInputStream file;
	
    protected Iterator<IBodyElement> iterator;

    protected List<GenericEntity> newEntities;
    protected List<GenericEntity> updateEntities;
    protected List<GenericEntity> deleteEntities;

    protected boolean end;

    protected boolean isEndTag(String tag){
        Pattern pattern = Pattern.compile(META_REGEXP, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(tag);
        if (matcher.find()){
            if (matcher.group().toLowerCase().contains("end")){
                return true;
            }
        }
        return false;
    }

	@Deprecated
    protected Iterator<IBodyElement> openDoc(String path) {
        try {
            file = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            Log.warn("File not found!", e);
            e.printStackTrace();
        }
        XWPFDocument xdoc = null;
        try {
            xdoc = new XWPFDocument(OPCPackage.open(file));
        } catch (IOException e) {
            Log.warn("IOException: ", e);
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            Log.warn("InvalidFormat: ", e);
            e.printStackTrace();
        }
        iterator = xdoc.getBodyElementsIterator();
        return iterator;
    }

    protected String getTextFromParagraph(IBodyElement element) {
        StringBuilder tmp = new StringBuilder();
        if (PARAGRAPH.equalsIgnoreCase(element.getElementType().name())) {
            XWPFParagraph paragraph = (XWPFParagraph) element;
            List<XWPFRun> lines = paragraph.getRuns();
            for (XWPFRun line : lines) {
                tmp.append(line.text());
            }
            return tmp.toString().trim();
        }
        return "";
    }

    protected void goToStart(String meta) {
        while (iterator.hasNext()) {
            IBodyElement element = iterator.next();
            if (getTextFromParagraph(element).equalsIgnoreCase(meta)) {
                Log.info("Meta is found! " + meta);
                return;
            }
        }
        Log.warn("Meta not found!");
    }

    protected boolean isTable(IBodyElement element) {
        if (element.getElementType().name().equalsIgnoreCase(TABLE))
            return true;
        return false;
    }

    protected List<List<String>> readRowsFromTable(XWPFTable table) {
        StringBuilder tmp;
        List<List<String>> tableText = new LinkedList<>();
        List<XWPFTableRow> rows = table.getRows();
        for (XWPFTableRow row : rows) {
            List<String> rowText = new LinkedList<>();
            List<XWPFTableCell> cells = row.getTableCells();
            for (XWPFTableCell cell : cells) {
                tmp = new StringBuilder();
                List<XWPFParagraph> paragraphs = cell.getParagraphs();
                for (XWPFParagraph paragraph : paragraphs) {
                    tmp.append(getTextFromParagraph(paragraph));
                }
                rowText.add(tmp.toString());
            }
            tableText.add(rowText);
        }
        return tableText;
    }
	
	protected int checkID(String str){
		Pattern pattern = Pattern.compile(REMOVE_ID_REGEXP, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		if (matcher.find()){
			return 2;
		}
		
		pattern = Pattern.compile(ID_REGEXP, Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(str);
		if (matcher.find()){
			return 1;
		} else {
			return 0;
		}
		
	}
	
	protected Long getID(String str){
		Pattern pattern = Pattern.compile(ID_REGEXP, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		if (matcher.find()){
			String tmp = matcher.group();
			if (tmp.length() > 3){
				tmp = tmp.substring(3);
				return Long.parseLong(tmp);
			}
		}
		return null;
	}
	
	protected int checkTableID(String str){
		Pattern pattern = Pattern.compile(TABLE_REMOVE_ID_REGEXP, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		
		if ( matcher.find() ){
			return 2;
		}
		
		pattern = Pattern.compile(TABLE_ID_REGEXP, Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(str);
		
		if (matcher.find()){
			return 1;
		} else {
			return 0;
		}
		
	}
	
	protected Long getTableID(String str){
		Pattern pattern = Pattern.compile(TABLE_ID_REGEXP, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		if (matcher.find()) {
            return Long.parseLong(matcher.group());
        } else {
            return null;
        }
	}
	
	public void parse(Iterator<IBodyElement> iterator){
		this.iterator = iterator;
	}

    public List<GenericEntity> getNewEntities() {
        return newEntities;
    }

    public List<GenericEntity> getUpdateEntities() {
        return updateEntities;
    }

    public List<GenericEntity> getDeleteEntities() {
        return deleteEntities;
    }

    public BaseParser() {
        end = false;
        newEntities = new LinkedList<>();
        updateEntities = new LinkedList<>();
        deleteEntities = new LinkedList<>();
    }
	@Deprecated
    public BaseParser(String path) {
        newEntities = new LinkedList<>();
        updateEntities = new LinkedList<>();
        deleteEntities = new LinkedList<>();
      openDoc(path);
    }
	
	public BaseParser(Iterator<IBodyElement> iterator){
        this();
		this.iterator = iterator;
	}
}