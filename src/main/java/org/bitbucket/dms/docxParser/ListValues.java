package org.bitbucket.dms.docxParser;


import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Deprecated
public class ListValues {
    /*
    private static Logger Log = LoggerFactory.getLogger(Parser.class.getName());

    private final static String PARAGRAPH = "PARAGRAPH";
    private final static String TABLE = "TABLE";

    private final static String META_OBJECT_TYPE = "{META: List Value Type}";

    private static Iterator<IBodyElement> OpenDoc(String path){
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            Log.warn("File not found!", e);
            e.printStackTrace();
        }
        XWPFDocument xdoc = null;
        try {
            xdoc = new XWPFDocument(OPCPackage.open(fis));
        } catch (IOException e) {
            Log.warn("IOException: ", e);
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            Log.warn("InvalidFormat: ", e);
            e.printStackTrace();
        }
        return xdoc.getBodyElementsIterator();

    }

    private static String GetTextFromParagraph(IBodyElement element){
        StringBuilder tmp = new StringBuilder();
        if (PARAGRAPH.equalsIgnoreCase(element.getElementType().name())) {
            XWPFParagraph paragraph = (XWPFParagraph) element;
            List<XWPFRun> lines = paragraph.getRuns();
            for (XWPFRun line : lines) {
                tmp.append(line.text());
            }
            return tmp.toString();
        }
        return null;
    }


    private static void GoToStart(String meta, Iterator<IBodyElement> iterator){
        while (iterator.hasNext()){
            IBodyElement element = iterator.next();
            if (GetTextFromParagraph(element).equalsIgnoreCase(meta)) {
                Log.debug("Meta is found! " + meta);
                return;
            }
        }
        Log.warn("Meta not found!");
    }

    private static String GeObjectTypeDescription(Iterator<IBodyElement> iterator){
        while (iterator.hasNext()){
            if(GetTextFromParagraph(iterator.next()).equalsIgnoreCase("Description")){
                return GetTextFromParagraph(iterator.next());
            }
        }
        Log.warn("Description not found!");
        return null;
    }

    private static boolean IsTable(IBodyElement element){
        if (element.getElementType().name().equalsIgnoreCase(TABLE))
            return true;
        return false;
    }

    private static List<List<String>> ReadRowsFromTable(XWPFTable table){
        StringBuilder tmp;
        List<List<String>> tableText = new LinkedList<>();
        List<XWPFTableRow> rows = table.getRows();
        for (XWPFTableRow row: rows){
            List<String> rowText = new LinkedList<>();
            List<XWPFTableCell> cells = row.getTableCells();
            for(XWPFTableCell cell: cells){
                tmp = new StringBuilder();
                List<XWPFParagraph> paragraphs = cell.getParagraphs();
                for(XWPFParagraph paragraph: paragraphs){
                    tmp.append(GetTextFromParagraph(paragraph));
                }
                rowText.add(tmp.toString());
            }
            tableText.add(rowText);
        }
        return tableText;
    }

    private static String GetObjectTypeValues(Iterator<IBodyElement> iterator){
        while (!GetTextFromParagraph(iterator.next()).equalsIgnoreCase("Value")){ }
        IBodyElement element = iterator.next();
        while (!IsTable(element)) { element = iterator.next(); }
        List<List<String>> properties = ReadRowsFromTable((XWPFTable)element);

        StringBuilder tmp = new StringBuilder();
        int k = 0;
        for(List<String> row: properties){
            if(k == 0){
                k++;
                continue;
            }
            tmp.append(row.get(0)).append(": ").append(row.get(1)).append("; ");
        }
        return tmp.toString();
    }

   

    private static Type GetObjectType(Iterator<IBodyElement> iterator){
        Type type = new Type();

        type.setName(GetTextFromParagraph(iterator.next()));
        type.setDescription(GeObjectTypeDescription(iterator));
        type.setProperties(GetObjectTypeValues(iterator));
        Log.debug(type.toString());
        
//        Log.debug(GetObjectTypeAttributes(iterator).toString());
        return type;
    }

    public static void ParseObjectTypes(String path){
        Iterator<IBodyElement> iterator = OpenDoc(path);

        GoToStart(META_OBJECT_TYPE, iterator);
//        while(iterator.hasNext()) {
            Type type = GetObjectType(iterator);
//        }

    }

    private ListValues(){}
    */
}
