package org.bitbucket.dms.docxParser;

import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.bitbucket.dms.dao.DataTypeDAO;
import org.bitbucket.dms.dao.ObjectDAO;
import org.bitbucket.dms.dao.TypeDAO;
import org.bitbucket.dms.model.ObjectModel;
import org.bitbucket.dms.model.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJBException;
import javax.management.openmbean.OpenMBeanAttributeInfo;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ObjectsParser extends BaseParser{
	
	private static Logger Log = LoggerFactory.getLogger(ObjectsParser.class.getName());

	private String getObjName(){
		String tmpString;
		do {
			if (!iterator.hasNext()){
				end = true;
				return null;
			}
			tmpString = getTextFromParagraph(iterator.next());
		} while ((tmpString == null || tmpString.equals("")) && !end ) ;
		return tmpString;
	}
	private List<List<String>> getParams(String str){
		List<String> tmpParams = new LinkedList<>();
		List<List<String>> params = new LinkedList<>();

		Pattern pattern = Pattern.compile("[.[^;]]+;");
		Matcher matcher = pattern.matcher(str);
		while(matcher.find()){
			tmpParams.add(matcher.group());
			tmpParams.add(str.substring(matcher.start(), matcher.end()));
			str = str.substring(matcher.start());
			matcher = pattern.matcher(str);
		}

		for (String tmp: tmpParams){
			LinkedList<String> tmpString = new LinkedList<>();
			pattern = Pattern.compile("[\\s\\w\\d]+,");
			matcher = pattern.matcher(tmp);
			if (matcher.find()){
				tmpString.add(tmp.substring(matcher.start(), matcher.end() - 1));
				tmpString.add(tmp.substring(matcher.end() + 1));
			} else {
				tmpString.add(tmp);
			}
			params.add(tmpString);

		}

		return params;
	}

	private void setParams(ObjectModel object, List<List<String>> params){
		TypeDAO typeDAO;
		ObjectDAO objectDAO;

		try {
			typeDAO = (TypeDAO) new InitialContext().lookup("java:module/TypeDAO");
			objectDAO = (ObjectDAO) new InitialContext().lookup("java:module/ObjectDAO");
			if (typeDAO==null) Log.error("******************** TypeDAO IS NULL!!!!! ****************");

		} catch (NamingException e) {
			throw new EJBException(e);
		}
		for(List<String> param: params){
			if (param.size() == 2){
				if (checkID(param.get(0)) == 1){
					if (param.get(1).contains("Type")){
						object.setType(typeDAO.getById(Type.class, Long.parseLong(param.get(0))));
					} else if(param.get(1).contains("Parent")){
						object.setParent(objectDAO.getById(ObjectModel.class, Long.parseLong(param.get(0))));
					} else if (param.get(1).contains("Project")){
						object.setProjectID(objectDAO.getById(ObjectModel.class, Long.parseLong(param.get(0))));
					}
				} else {
					if (param.get(0).contains("Type")){
						object.setType(typeDAO.getById(Type.class, Long.parseLong(param.get(1))));
					} else if(param.get(0).contains("Parent")){
						object.setParent(objectDAO.getById(ObjectModel.class, Long.parseLong(param.get(1))));
					} else if (param.get(0).contains("Project")){
						object.setProjectID(objectDAO.getById(ObjectModel.class, Long.parseLong(param.get(1))));
					}
				}
			} else {
				if (checkID(param.get(0)) > 0){
					object.setId(getID(param.get(0)));
					if (checkID(param.get(0)) == 2){
						deleteEntities.add(object);
					} else {
						updateEntities.add(object);
					}
				} else {
					newEntities.add(object);
				}

				if (param.get(0).contains("Type=")){
					object.setType(typeDAO.getByName(param.get(0).substring(6)).get(0));
				} else if(param.get(0).contains("Parent=")){
					object.setParent(objectDAO.getByName(param.get(0).substring(8)).get(0));
				} else if (param.get(0).contains("Project=")){
					object.setParent(objectDAO.getByName(param.get(0).substring(9)).get(0));
				} else if (param.get(0).contains("Order=")){
					object.setOrderID(Long.parseLong(param.get(0).substring(7)));
				}
			}
		}
	}

	private void getDescriptionOrFields(ObjectModel object){
		do{
			String tmpString;
			do {
				if (!iterator.hasNext()){
					end = true;
					return;
				}
				tmpString = getTextFromParagraph(iterator.next());
			} while ((tmpString == null || tmpString.equals("")) && !end ) ;

			if(tmpString.equalsIgnoreCase("Description")){
				do {
					tmpString = getTextFromParagraph(iterator.next());
				}while ((tmpString == null || tmpString.equals("")) && !end);
				object.setDescription(tmpString);
				break;
			} else {
				setParams(object, getParams(tmpString));
			}
		} while(!end);
	}

	private ObjectModel getObject(){
		ObjectModel object = new ObjectModel();
		object.setName(getObjName());
		if (object.getName() == null){
			return null;
		}
		getDescriptionOrFields(object);

		//TODO: sdelaite kto-nibud'
		return object;
	}
	
	@Override
	public void parse(Iterator<IBodyElement> iterator){
		super.parse(iterator);

		ObjectModel tmpObject;

		do {
			tmpObject = getObject();
		} while (!end && iterator.hasNext());
		
	}
	
	public ObjectsParser() {
		super();
    }

	@Deprecated
    public ObjectsParser(String path) {
		super(path);
    }
	
	public ObjectsParser(Iterator<IBodyElement> iterator){
		super(iterator);
	}
}