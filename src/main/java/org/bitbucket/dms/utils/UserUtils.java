package org.bitbucket.dms.utils;

import org.bitbucket.dms.dao.AttributeDAO;
import org.bitbucket.dms.dao.GenericDAO;
import org.bitbucket.dms.dao.ObjectDAO;
import org.bitbucket.dms.dao.ValueDAO;
import org.bitbucket.dms.model.Attribute;
import org.bitbucket.dms.model.ObjectModel;
import org.bitbucket.dms.model.Type;
import org.bitbucket.dms.model.Value;
import org.bitbucket.dms.views.RegisterView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;

/**
 * Created by pdi75 on 15.05.2016.
 */
@Stateless
public class UserUtils {

    @EJB
    private GenericDAO dao;
    @EJB
    private AttributeDAO attributeDAO;
    @EJB
    private ObjectDAO objectDAO;
    @EJB
    private ValueDAO valueDAO;
    private Logger log = LoggerFactory.getLogger(UserUtils.class);

    public boolean userIsExist(String login) {
        return !valueDAO.getByStringValueAndByAttributeId(login, 6L).isEmpty();
    }


    public void registerUser(String login, String password) {
        if (userIsExist(login)) {
            log.info("Attempt to create user that already exist");
        } else {
            log.info("adduser call");
            ObjectModel userObject = new ObjectModel();
            userObject.setType((Type) dao.getById(Type.class, 8L));
            userObject.setName(login);
            userObject.setParent((ObjectModel) dao.getById(ObjectModel.class, 7L));
            userObject.setProjectID((ObjectModel) dao.getById(ObjectModel.class, 7L));
            userObject.setDescription("user ");
            userObject.setOrderID(1L);
            dao.add(userObject);
            log.info("Object ID after persist " + userObject.getId());
            List<Attribute> loginAttributes = attributeDAO.getByName("Login");
            log.info("Attributes with name login: " + loginAttributes.size());
            List<Attribute> passwordAttributes = attributeDAO.getByName("Password");
            log.info("Attributes with name password: " + passwordAttributes.size());
            if (loginAttributes.size() == 1 && passwordAttributes.size() == 1) {
                Value loginValueEntity = new Value();
                loginValueEntity.setAttribute(loginAttributes.get(0));
                loginValueEntity.setStringValue(login);
                loginValueEntity.setObj(userObject);
                Value passwordValueEntity = new Value();
                passwordValueEntity.setAttribute(passwordAttributes.get(0));
                passwordValueEntity.setStringValue(md5Hasher.getMd5Hash(password));
                passwordValueEntity.setObj(userObject);
                dao.add(loginValueEntity);
                dao.add(passwordValueEntity);
            } else log.error("Database is corrupt");

        }
    }
    public boolean checkUserAuth (String login,String password){
        if (userIsExist(login)){
            Value userValue=valueDAO.getByStringValue(login).get(0);
            Value passwordValue=valueDAO.getByObjectIdAndByAttributeId(userValue.getObj().getId(),7L).get(0);
            if (md5Hasher.getMd5Hash(password).equals(passwordValue.getStringValue())) return true;
        }
        return false;
    }
    public ObjectModel getUserObject(String login)
    {
        List <ObjectModel> objectsList;
        objectsList=objectDAO.getByNameAndTypeId(login, 8L);
        if(!objectsList.isEmpty()) return objectsList.get(0);
        return null;
    }
}
