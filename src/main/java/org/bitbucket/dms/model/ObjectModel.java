package org.bitbucket.dms.model;

import javax.persistence.*;
import org.apache.commons.lang3.builder.ToStringBuilder;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@NamedQuery(name="Object.getAll", query="SELECT a FROM ObjectModel a")
@Table(name="objects")
public class ObjectModel extends GenericEntity{

    @Id
    @Column(name="object_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private Type type;
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private ObjectModel parent;
    @Column(name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(name = "project_id")
    private ObjectModel projectID;
    @Column(name = "order_id")
    private Long orderID;

    public ObjectModel(Long id, Type type, String name, ObjectModel parent,
                       String description, ObjectModel projectID, Long orderID){
        this.id = id;
        this.type = type;
        this.name = name;
        this.parent = parent;
        this.description = description;
        this.projectID = projectID;
        this.orderID = orderID;
    }

    public ObjectModel(){}

    public Long getId(){
        return this.id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Type getType(){
        return type;
    }
    public void setType(Type type){
        this.type = type;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    public ObjectModel getParent(){
        return this.parent;
    }
    public void setParent(ObjectModel parent){
        this.parent = parent;
    }

    public String getDescription(){
        return this.description;
    }
    public void setDescription(String description){
        this.description = description;
    }

    public ObjectModel getProjectID(){
        return this.projectID;
    }
    public void setProjectID(ObjectModel projectID){
        this.projectID = projectID;
    }

    public Long getOrderID(){
        return this.orderID;
    }
    public void setOrderID(Long orderID) {
        this.orderID = orderID;
    }

    @Override
    public String toString(){
        return ToStringBuilder.reflectionToString(this);
    }
}
