package org.bitbucket.dms.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.List;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@NamedQuery(name="Type.getAll", query="SELECT t FROM Type t")
@Table(name="types")
public class Type extends GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="type_id")
    //@GeneratedValue
    private Long id;
    private String name;
    private String description;
    private String properties;


    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable(
            name="attr_types"
            , joinColumns={
            @JoinColumn(name="type_id")
    }
            , inverseJoinColumns={
            @JoinColumn(name="attribute_id")
    }
    )
    private List<Attribute> attributes;




    public Type() {
    }

    public Type(Long id, String name, String description, String properties) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.properties = properties;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public List<Attribute> getAttributes(){
        return attributes;
    }
    public void setAttributes(List<Attribute> attributes){
        this.attributes = attributes;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String surname) {
        this.description = surname;
    }

    public String getProperties() {
        return properties;
    }
    public void setProperties(String properties){
        this.properties = properties;
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Type type = (Type) o;

        if (id != null ? !id.equals(type.id) : type.id != null) return false;
        if (name != null ? !name.equals(type.name) : type.name != null) return false;
        if (description != null ? !description.equals(type.description) : type.description != null) return false;
        if (properties != null ? !properties.equals(type.properties) : type.properties != null) return false;
        return attributes != null ? attributes.equals(type.attributes) : type.attributes == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (properties != null ? properties.hashCode() : 0);
        result = 31 * result + (attributes != null ? attributes.hashCode() : 0);
        return result;
    }
}