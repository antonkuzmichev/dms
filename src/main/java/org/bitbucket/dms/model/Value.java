package org.bitbucket.dms.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NamedQuery(name = "Values.getAll", query = "SELECT v FROM Value v")
@Table(name = "values")
public class Value extends GenericEntity{
    @Id
    @Column(name = "value_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "object_id")
    private ObjectModel obj;
    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "attribute_id")
    private Attribute attribute;
    @Column(name = "value_int")
    private Long numValue;
    @Column(name = "value_string")
    private String stringValue;
    @Column(name = "value_date")
    private Date dateValue;

    @ManyToOne
    @JoinColumn(name = "value_listvalue_id")
    private ListValue listValue;



    public Value() {
    }

    public Value(ObjectModel obj, Attribute attribute, Long numValue, String stringValue, Date dateValue) {
        this.obj = obj;
        this.attribute = attribute;
        this.numValue = numValue;
        this.stringValue = stringValue;
        this.dateValue = dateValue;
    }

    public String getUniValue() {

        Long type = attribute.getDataType().getId();
        if (type == 1L) return numValue.toString();
        if (type == 2L) return stringValue;
        if (type == 3L) return dateValue.toString();
        if (type == 4L) return listValue.getValue(); //"here must be displayed value from list";
        return "value is missing";

    }

    public Long getId() {
        return id;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public Long getNumValue() {
        return numValue;
    }

    public void setNumValue(Long numValue) {
        this.numValue = numValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public ObjectModel getObj() {
        return obj;
    }

    public void setObj(ObjectModel obj) {
        this.obj = obj;
    }
}
