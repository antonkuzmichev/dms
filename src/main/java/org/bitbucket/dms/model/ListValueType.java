package org.bitbucket.dms.model;

import javax.persistence.*;

import java.util.List;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@NamedQuery(name = "ListValueType.getAll", query = "SELECT l FROM ListValueType l")

@Table(name = "list_value_type")

public class ListValueType extends GenericEntity{
	@Id
    @Column(name = "list_value_type_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "listValueType",fetch = FetchType.EAGER)
    private List<ListValue> listValues;

    public List<ListValue> getListValues() {
    	
        return listValues;
    }

    public void setListValues(List<ListValue> listValues) {
        this.listValues = listValues;
    }

    public ListValueType(Long id, String name) {
        this.id = id;
        this.name = name;

    }

    public ListValueType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long listValueTypeId) {
        this.id = listValueTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
    	
        return  "list_value_type_id " + getId() +
        		" name " + getName();
    }
    public boolean equals(Object  o) {
        ListValueType a=(ListValueType) o;
        return a.getId().equals(this.getId());
    }
}
