package org.bitbucket.dms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery(name = "AttrListValue.getAll", query = "SELECT a FROM AttrListValue a")
@Table(name = "attr_list_value")
public class AttrListValue {

    @Id
    @Column(name = "attribute_id")
    private Long attributeId;
    @Column(name = "list_value_type_id")
    private Long listValueTypeId;
    
    public AttrListValue(Long attributeId, Long listValueTypeId) {
        this.attributeId = attributeId;
        this.listValueTypeId = listValueTypeId;
    }
    
    public AttrListValue() {}
    
    public Long getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Long attributeId) {
        this.attributeId = attributeId;
    }
    
    public Long getListValueTypeId() {
        return listValueTypeId;
    }

    public void setListValueTypeId(Long listValueTypeId) {
        this.listValueTypeId = listValueTypeId;
    }
    

    @Override
    public String toString() {
        return "attributeId : " + getAttributeId() +
                ", listValueTypeId : " + getListValueTypeId();
    }
}
