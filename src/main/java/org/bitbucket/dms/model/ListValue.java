package org.bitbucket.dms.model;

import javax.persistence.*;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.math.BigInteger;

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@NamedQuery(name="ListValue.getAll", query="SELECT l FROM ListValue l")
@Table(name="list_values")

public class ListValue extends GenericEntity {
    @Id
    @Column(name="list_value_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long listValueId;

    @ManyToOne(fetch=FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name="list_value_type_id", insertable = true, updatable = true)
    private ListValueType listValueType;

    @Column(name="value")
    private String value;

    public ListValue(Long listValueId, BigInteger listValueTypeId, String value) {
    this.listValueId = listValueId;
    //this.listValueType = listValueTypeId;
    this.value = value;

    }

    public ListValue() {
    }

    public Long getListValueId() {
        return listValueId;
    }
    public void setListValueId(Long listValueId) {
        this.listValueId = listValueId;
    }

    public ListValueType getListValueType() {
        return listValueType;
    }

    public void setListValueType(ListValueType listValueType) {
        this.listValueType = listValueType;
    }

    public String getValue(){
    	return value;
    }
    public void setValue(String value){
    	this.value = value;
    }
   
    @Override
    public String toString(){
		 return getValue();
    	
    	
    }
}
