package org.bitbucket.dms.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;


/**
 * Author: Popov Dmitry
 */


@Entity
@NamedQuery(name="DataType.getAll", query="SELECT a FROM DataType a")
@Table(name="data_types")
public class DataType {

    @Id
    private Long id;
    private String name;

    public DataType(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public DataType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("name", name).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataType dataType = (DataType) o;

        if (id != null ? !id.equals(dataType.id) : dataType.id != null) return false;
        return name != null ? name.equals(dataType.name) : dataType.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}