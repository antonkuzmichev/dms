package org.bitbucket.dms.model;

import javax.persistence.*;

@Entity
@NamedQuery(name="Reference.getAll", query="SELECT a FROM Reference a")
@Table(name="object_references")
public class Reference extends GenericEntity{


    @Id
    @Column(name="ref_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "object_id")
    private ObjectModel object;

    @ManyToOne
    @JoinColumn(name = "object_ref_id")
    private ObjectModel reference;

    @ManyToOne
    @JoinColumn(name= "ref_attribute")
    private Attribute attribute;

    @Column(name = "ref_order")
    private Long order;

    public Reference() {
    }

    public Reference(ObjectModel object, ObjectModel reference, Attribute attribute, Long order) {
        this.object = object;
        this.reference = reference;
        this.attribute = attribute;
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public ObjectModel getObject() {
        return object;
    }

    public void setObject(ObjectModel object) {
        this.object = object;
    }

    public ObjectModel getReference() {
        return reference;
    }

    public void setReference(ObjectModel reference) {
        this.reference = reference;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    @Override
    public String toString(){
        return "Id : " + getId() +
                ", Object : " + getObject() +
                ", reference : " + getReference() +
                ", attribute : " + getAttribute() +
                ", order : " + getOrder();
    }
}
