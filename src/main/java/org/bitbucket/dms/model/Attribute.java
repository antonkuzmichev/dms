package org.bitbucket.dms.model;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.List;

/**
 * Author: Popov Dmitry
 */


@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
//@NamedQuery(name="Attribute.getAll", query="SELECT a FROM Attribute a")
@Table(name="attributes")
public class Attribute extends GenericEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="attribute_id")
    private Long id;
    private String name;

    @Column(name="description")
    private String description;

    @ManyToOne
    @JoinColumn(name="atr_type")
    private DataType dataType;

    @Column(name = "properties")
    private String properties;


    @ManyToOne
    @JoinTable(
            name="attr_list_value"
            , joinColumns={
            @JoinColumn(name="attribute_id")
    }
            , inverseJoinColumns={
            @JoinColumn(name="list_value_type_id")
    }
    )
    private ListValueType listValueType;


    public Attribute(String name, String description, DataType dataType, String properties, ListValueType listValueType) {
        this.name = name;
        this.description = description;
        this.dataType = dataType;
        this.properties = properties;
        this.listValueType = listValueType;
    }

    public Attribute(String name, String description, DataType dataType, String properties) {
        this.name = name;
        this.description = description;
        this.dataType = dataType;
        this.properties = properties;
    }

    public Attribute() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public String getTypeName ()
    {
        return dataType.getName();
    }

    public void setProperties(String properties){
        this.properties = properties;
    }
    public String getProperties(){
        return properties;
    }

    public ListValueType getListValueType() {
        return listValueType;
    }

    public void setListValueType(ListValueType listValueType) {
        this.listValueType = listValueType;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dataType=" + dataType +
                ", properties='" + properties + '\'' +
                ", listValueType=" + listValueType +
                '}';
    }

    public boolean equals(Object  o) {
        Attribute a=(Attribute) o;
        return a.getId().equals(this.getId());
    }
    public int hashCode() {
        // you pick a hard-coded, randomly chosen, non-zero, odd number
        // ideally different for each class
        return new HashCodeBuilder(17, 37).
                append(id).
                append(name).
                append(description).
                append(properties).
                toHashCode();
    }
}