package org.bitbucket.dms.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.List;


/**
 * Author: Popov Dmitry
 */


@Entity
@Table(name = "documents_def")
public class Doc {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private String hash;


    @ManyToMany
    @JoinTable(
            name = "docs_changes"
            , joinColumns = {
            @JoinColumn(name = "doc_id")
    }
            , inverseJoinColumns = {
            @JoinColumn(name = "attribute_id")
    }
    )
    private List<Attribute> attributes;


    @ManyToMany
    @JoinTable(
            name = "docs_changes"
            , joinColumns = {
            @JoinColumn(name = "doc_id")
    }
            , inverseJoinColumns = {
            @JoinColumn(name = "type_id")
    }
    )
    private List<Type> types;

    @ManyToMany
    @JoinTable(
            name = "docs_changes"
            , joinColumns = {
            @JoinColumn(name = "doc_id")
    }
            , inverseJoinColumns = {
            @JoinColumn(name = "list_value_type_id")
    }
    )
    private List<ListValueType> listValueTypes;

    @ManyToMany
    @JoinTable(
            name = "docs_changes"
            , joinColumns = {
            @JoinColumn(name = "doc_id")
    }
            , inverseJoinColumns = {
            @JoinColumn(name = "list_value_id")
    }
    )
    private List<ListValue> listValues;


    public Doc(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Doc() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    public List<ListValueType> getListValueTypes() {
        return listValueTypes;
    }

    public void setListValueTypes(List<ListValueType> listValueTypes) {
        this.listValueTypes = listValueTypes;
    }

    public List<ListValue> getListValues() {
        return listValues;
    }

    public void setListValues(List<ListValue> listValues) {
        this.listValues = listValues;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).
                append("name", name).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Doc dataType = (Doc) o;

        if (id != null ? !id.equals(dataType.id) : dataType.id != null) return false;
        return name != null ? name.equals(dataType.name) : dataType.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}