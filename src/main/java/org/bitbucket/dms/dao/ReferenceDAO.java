package org.bitbucket.dms.dao;

import org.bitbucket.dms.model.Reference;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by pdi75 on 27.04.2016.
 */
@Stateless
public class ReferenceDAO extends GenericDAO<Reference, Long> {


    public List<Reference> getByObjectId(Long objectId) {
       Query q = getEmf().createQuery("SELECT r FROM Reference r where r.object.id="+objectId, Reference.class);
        return q.getResultList();
    }


}
