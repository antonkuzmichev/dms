package org.bitbucket.dms.dao;


import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.bitbucket.dms.dao.ListValueTypeDAO;
import org.bitbucket.dms.model.ListValueType;

@Stateless
public class ListValueTypeDAO {
	
    @PersistenceContext

    private EntityManager entityManager;

    public void add(ListValueType listValueTypes) {
        entityManager.persist(listValueTypes);
    }

    public void update(ListValueType listValueTypes) {
        entityManager.merge(listValueTypes);
    }

    public void delete(ListValueType listValueTypes) {
        entityManager.remove(
                entityManager.contains(listValueTypes) ? listValueTypes : entityManager.merge(listValueTypes)
        );
    }

    public ListValueType getByListValueTypeId(Long listValueTypeId) {
        return entityManager.find(ListValueType.class, listValueTypeId);
    }

    public List<ListValueType> getAll() {
        return entityManager.createNamedQuery("ListValueType.getAll", ListValueType.class)
                .getResultList();
    }

    public List<ListValueType> getListValue(Long listValueTypeId) {
        Query q = entityManager.createQuery("select lv from ListValue lv " +
                " where lv.listValueTypeId = :id");
        q.setParameter("id", listValueTypeId);
        return  q.getResultList();
    }
}