package org.bitbucket.dms.dao;

import org.bitbucket.dms.model.Attribute;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by pdi75 on 14.05.2016.
 */
@Stateless
public class AttributeDAO extends GenericDAO<Attribute,Long> {


    public List<Attribute> getByName(String name) {
        Query q = getEmf().createQuery("SELECT r FROM Attribute r where r.name= :name", Attribute.class);
        q.setParameter("name",name);
        return q.getResultList();
    }
}
