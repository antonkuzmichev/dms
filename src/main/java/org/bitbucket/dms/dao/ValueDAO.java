package org.bitbucket.dms.dao;

import org.bitbucket.dms.model.Reference;
import org.bitbucket.dms.model.Value;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by pdi75 on 27.04.2016.
 */
@Stateless
public class ValueDAO extends GenericDAO<Value, Long> {


    public List<Value> getByObjectId(Long objectId) {
       Query q = getEmf().createQuery("SELECT r FROM Value r where r.obj.id= :objId", Value.class);
        q.setParameter("objId",objectId);
        return q.getResultList();
    }

    public List<Value> getByStringValue (String value) {
        Query q = getEmf().createQuery("SELECT r FROM Value r where r.stringValue= :val", Value.class);
        q.setParameter("val",value);
        return q.getResultList();
    }

    public List<Value> getByStringValueAndByAttributeId (String value, Long attributeId) {
        Query q = getEmf().createQuery("SELECT r FROM Value r where r.stringValue= :val and r.attribute.id= :attrId", Value.class);
        q.setParameter("val",value);
        q.setParameter("attrId",attributeId);
        return q.getResultList();
    }
    public List<Value> getByObjectIdAndByAttributeId (Long objectId, Long attributeId) {
        Query q = getEmf().createQuery("SELECT r FROM Value r where r.obj.id= :objId and r.attribute.id= :attrId", Value.class);
        q.setParameter("objId",objectId);
        q.setParameter("attrId",attributeId);
        return q.getResultList();
    }


}
