package org.bitbucket.dms.dao;

import org.bitbucket.dms.model.DataType;
import org.bitbucket.dms.model.Type;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by pdi75 on 27.04.2016.
 */
@Dependent
@Stateless
public class DataTypeDAO extends GenericDAO<DataType, Long> {


    public List<DataType> getByName(String name) {
        Query q = getEmf().createQuery("SELECT r FROM DataType r where r.name= :name", DataType.class);
        q.setParameter("name",name);
        return q.getResultList();
    }

}
