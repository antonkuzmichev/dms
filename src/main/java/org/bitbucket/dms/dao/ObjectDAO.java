package org.bitbucket.dms.dao;

import org.bitbucket.dms.model.ObjectModel;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by pdi75 on 14.05.2016.
 */
@Stateless
public class ObjectDAO extends GenericDAO<ObjectModel,Long> {


    public List<ObjectModel> getByName(String name) {
        Query q = getEmf().createQuery("SELECT r FROM ObjectModel r where r.name= :name", ObjectModel.class);
        q.setParameter("name",name);
        return q.getResultList();
    }

    public List<ObjectModel> getByNameAndTypeId (String name, Long typeId)
    {
        Query q = getEmf().createQuery("SELECT r FROM ObjectModel r where r.name= :name and r.type.id= :typeId", ObjectModel.class);
        q.setParameter("name",name);
        q.setParameter("typeId",typeId);
        return q.getResultList();

    }

}
