package org.bitbucket.dms.dao;

import org.bitbucket.dms.model.Type;
import org.bitbucket.dms.model.Value;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by pdi75 on 27.04.2016.
 */
@Stateless
public class TypeDAO extends GenericDAO<Type, Long> {


    public List<Type> getByName(String name) {
        Query q = getEmf().createQuery("SELECT r FROM Type r where r.name= :name", Type.class);
        q.setParameter("name",name);
        return q.getResultList();
    }

}
