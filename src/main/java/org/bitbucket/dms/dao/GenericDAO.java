package org.bitbucket.dms.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


/**
 * Author: Popov Dmitry
 */


@SuppressWarnings("unchecked")
@Stateless
public class GenericDAO<T, PK> {



    @PersistenceContext
    private EntityManager emf;

    public T getById(Class<T> type, PK id) {return emf.find(type, id);}



    public T add (T t) {
        emf.persist(t);
        return t;
    }

    public T update(T t) {
        return emf.merge(t);
    }

    public void delete(T t) {
        emf.remove(emf.merge(t));
    }

    public List<T> getAll(Class<T> type) {
        Query q = emf.createQuery("SELECT r FROM " + type.getName() + " r", type);
        return q.getResultList();
    }


    protected EntityManager getEmf() {
        return emf;
    }

    protected void setEmf(EntityManager emf) {
        this.emf = emf;
    }
}

